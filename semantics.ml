(*
    This file is part of 'squall2sparql' <http://www.irisa.fr/LIS/softwares/squall/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

type uri = string
type var = string
type term =
  | Var of var
  | Uri of uri
  | Literal of string
  | GraphLiteral of var list * triple list
  | Cons of term * term
and triple = term * term * term


let disjoint xs1 xs2 = not (List.exists (fun x1 -> List.mem x1 xs2) xs1)

let option_map f = function
  | None -> None
  | Some x -> Some (f x)

let rec fold_while : ('a -> 'a option) -> 'a -> 'a =
  fun f e ->
    match f e with
    | None -> e
    | Some e' -> fold_while f e'

let rec merge lf =
  match merge_aux (List.rev lf) (`True,[]) with
  | `True, [] -> `True
  | f, [] -> f
  | `True, [f] -> f
  | `True, l -> `And l
  | f, l -> `And (f::l)
and merge_aux rev_lf (facc,lacc) =
  match rev_lf with
  | [] -> facc, lacc
  | (`And lf1)::rev_lf1 ->
      merge_aux (List.rev lf1 @ rev_lf1) (facc,lacc)
  | f1::rev_lf1 ->
      match merge_bin f1 facc with
      | None -> merge_aux rev_lf1 (f1,facc::lacc)
      | Some f1acc -> merge_aux rev_lf1 (f1acc,lacc)
and merge_bin f1 f2 =
  match f1, f2 with
  | `True, _ -> Some f2
  | _, `True -> Some f1
  | `Select _, _
  | _, `Select _ -> None
  | `Modif _, `Modif _ -> failwith "using 2 modifiers (e.g., superlatives) in a same clause is not allowed"
  | `Exists (xs1,f1), `Modif (op,x2,f2) ->
      option_map (fun f12 -> `Exists (xs1,`Modif (op,x2,f12))) (merge_bin f1 f2)
  | `Modif (op,x1,f1), `Exists (xs2,f2) ->
      option_map (fun f12 -> `Exists (xs2,`Modif (op,x1,f12))) (merge_bin f1 f2)
  | `Modif (op,x,f1), f2 ->
      option_map (fun f12 -> `Modif (op,x,f12)) (merge_bin f1 f2)
  | f1, `Modif (op,x,f2) ->
      option_map (fun f12 -> `Modif (op,x, f12)) (merge_bin f1 f2)
  | `Exists (xs1,f1), `Exists (xs2,f2) ->
      if disjoint xs1 xs2
      then option_map (fun f12 -> `Exists (xs1@xs2, f12)) (merge_bin f1 f2)
      else None
  | `Exists (xs1,f1), f2 ->
      option_map (fun f12 -> `Exists (xs1, f12)) (merge_bin f1 f2)
  | f1, `Exists (xs2,f2) ->
      option_map (fun f12 -> `Exists (xs2, f12)) (merge_bin f1 f2)
  | _ -> Some (bin_and_ f1 f2)
and bin_and_ f1 f2 =
  match f1, f2 with
  | `True, _ -> f2
  | _, `True -> f1
  | `And lf1, `And lf2 -> `And (lf1 @ lf2)
  | `And lf1, _ -> `And (lf1 @ [f2])
  | _, `And lf2 -> `And (f1 :: lf2)
  | _ -> `And [f1; f2]


let and_ = merge

let not_ = function
  | `Not f1 -> f1
  | f1 -> `Not f1

let bool =
  object (self)
    method id f = f
    method func op la res = `Func (op,la,res)
    method maybe_ f = `Option f
    method or_ lf = `Or lf
    method and_ lf = and_ lf
    method not_ f = not_ f
    method true_ = `True
    method exists lv f =
      if lv = []
      then f
      else merge [`Exists (lv,`True); f]
    method forall lv f1 f2 =
      let f1 = self#exists lv f1 in
      match f2 with
      | `Forall (f21,f22) ->
	  ( match merge_bin f1 f21 with
	  | Some f121 -> `Forall (f121,f22)
	  | None -> `Forall (f1,f2) )
      | _ -> `Forall (f1,f2)

  end


let bool0 =
  object (self)
    method private merge_xs_opt l =
      List.fold_right
	(fun xs_opt res ->
	  match xs_opt, res with
	  | None, _ -> res
	  | _, None -> xs_opt
	  | Some xs1, Some xs2 -> Some (xs1@xs2))
	l None
    method return f =
      None, (fun args mode -> f)
    method apply (xs_opt,s) transf =
      xs_opt, (fun args mode -> transf (s args mode))
    method apply_list la transf =
      let lxs_opt, ls = List.split la in
      self#merge_xs_opt lxs_opt, (fun args mode -> transf (List.map (fun s -> s args mode) ls))
    method map (xs_opt,s) transf =
      xs_opt, (fun args mode -> transf (s args mode))

    method id xss = self#map xss bool#id
    method func op la res = self#return (bool#func op la res)
    method maybe_ xss = self#apply xss bool#maybe_
    method or_ lxss = self#apply_list lxss bool#or_
    method and_ lxss = self#apply_list lxss bool#and_
    method not_ xss = self#apply xss bool#not_
    method true_ = self#return bool#true_
    method exists lv xss = self#apply xss (bool#exists lv)
    method forall lv (xs_opt1,s1) (xs_opt2,s2) =
      self#merge_xs_opt [xs_opt1; xs_opt2],
      (fun args mode ->
	bool#forall lv (s1 args `Interrogative) (s2 args mode))
    method the lv (xs_opt1,s1) (xs_opt2,s2) =
      self#merge_xs_opt [xs_opt1; xs_opt2],
      (fun args mode ->
	match mode with
	| `Interrogative -> bool#exists lv (bool#and_ [s1 args mode; s2 args mode])
	| `Affirmative -> bool#forall lv (s1 args `Interrogative) (s2 args mode))
    method whether (xs_opt,s) =
      self#merge_xs_opt [Some []; xs_opt],
      s
    method which x (xs_opt1,s1) (xs_opt2,s2) =
      self#merge_xs_opt [Some ([(x,s1)]); xs_opt1; xs_opt2],
      s2
    method select (xs_opt,s) =
      match xs_opt with
      | None -> s
      | Some xs ->
	  let lx, ls = List.split xs in
	  (fun args mode -> `Select (lx, bool#and_ (List.map (fun s -> s args `Interrogative) (ls@[s]))))
(*
    method relativize (xs_opt,s) =
      match xs_opt with
      | None
      | Some [] -> failwith "not a relative"
      | Some ((x1,_s1)::xs) ->
	  (fun x ->
	    (if xs = [] then None else Some xs),
	    (fun args mode -> bool#and_ [`Unify (x,x1); s args mode]))
*)
  end

let bool1 =
  object
    method id d x = bool0#id (d x)
    method func op la res x = bool0#func op la res
    method maybe_ d x = bool0#maybe_ (d x)
    method or_ ld x = bool0#or_ (List.map (fun d -> d x) ld)
    method and_ ld x = bool0#and_ (List.map (fun d -> d x) ld)
    method not_ d x = bool0#not_ (d x)
    method true_ x = bool0#true_
    method exists lv d x = bool0#exists lv (d x)
    method forall lv d1 d2 x = bool0#forall lv (d1 x) (d2 x)
    method the lv d1 d2 x = bool0#the lv (d1 x) (d2 x)
  end

let bool2 =
  object
    method id r x y = bool0#id (r x y)
    method func op la res x y = bool0#func op la res
    method maybe_ r x y = bool0#maybe_ (r x y)
    method or_ lr x y = bool0#or_ (List.map (fun r -> r x y) lr)
    method and_ lr x y = bool0#and_ (List.map (fun r -> r x y) lr)
    method not_ r x y = bool0#not_ (r x y)
    method true_ x y = bool0#true_
    method exists lv r x y = bool0#exists lv (r x y)
    method forall lv r1 r2 x y = bool0#forall lv (r1 x y) (r2 x y)
    method the lv r1 r2 x y = bool0#the lv (r1 x y) (r2 x y)
  end

(* let close s mode = s [] (fun t -> `True) mode *)
let init d x =
  let xs_opt, s = d x in
  xs_opt, (fun args mode -> s [] mode)
let arg uri z (xs_opt,s) =
  xs_opt,
  (fun args mode -> s ((uri,z)::args) mode)

(* pseudo predicates and prepositions *)
let pseudo_p1_belongs = "belongs"
let list_pseudo_p1 = [pseudo_p1_belongs]
let pseudo_p2_relates = "relates"
let list_pseudo_p2 = [pseudo_p2_relates]
let pseudo_prep_to = "to"
let list_pseudo_prep = [pseudo_prep_to]
let p2_sublist = "rdf:rest*"


let rec vars_triples_of_formula = function
  | `Triple (s,p,o) -> [], [(s,p,o)]
  | `And lf -> 
      let l = List.map vars_triples_of_formula lf in
      let llv, llt = List.split l in
      List.flatten llv, List.flatten llt
  | `Exists (xs,f) ->
      let lv, lt = vars_triples_of_formula f in
      let lv = List.fold_right (fun x lv -> match x with Var v -> v::lv | _ -> assert false) xs lv in
      lv, lt
  | _ -> failwith "Invalid graph literal: must be a conjunction of triples"
let graph_literal f =
  let lv, lt = vars_triples_of_formula f in
  GraphLiteral (lv, lt)

let nil = Uri "rdf:nil"
let cons x l = Cons (x,l)

let stat t s p o =
  None,
  (fun args mode ->
    let s, p, o, args =
      if p = Uri "rdf:type" && o = Uri pseudo_p1_belongs then
	if List.mem_assoc pseudo_prep_to args
	then s, Uri "rdf:type", List.assoc pseudo_prep_to args, List.remove_assoc pseudo_prep_to args
	else failwith "'belong(s)' expects a complement introduced by 'to'"
      else if p = Uri pseudo_p2_relates then
	if List.mem_assoc pseudo_prep_to args
	then o, s, List.assoc pseudo_prep_to args, List.remove_assoc pseudo_prep_to args
	else failwith "'relate(s)' expects a complement introduced by 'to'"
      else s, p, o, args in
    (try 
      let prep = List.find (fun prep -> List.mem_assoc prep args) list_pseudo_prep in
      failwith ("unexpected preposition '" ^ prep ^ "'")
    with _ -> ());
    match args with
    | [] -> `Triple (s,p,o)
    | args ->
	and_ (`Exists ([t], `True) ::
	      List.map
		(fun (uri,z) -> `Triple (t,Uri uri,z))
		(("rdf:subject",s)::("rdf:predicate",p)::("rdf:object",o)::args)))
let triple s p o = bool0#return (`Triple (s,p,o))
let a c t x = stat t x (Uri "rdf:type") c
let rel p t x y = stat t x p y
let unify x y = bool0#return (`Unify (x,y))
let matches (lre : string list) x = bool0#return (`Matches (x,lre))
let pred1 op x = bool0#return (`Pred1 (op,x))
let pred2 op x y = bool0#return (`Pred2 (op,x,y))
let func0 op x = bool0#func op [] x
let func1 op x y = bool0#func op [x] y
let proc1 op x = bool0#return (`Proc1 (op,x))
let graph x xss =
  let rec aux x = function
    | `Exists (xs,f1) -> `Exists (xs, aux x f1)
    | `Forall (f1,f2) -> `Forall (aux x f1, aux x f2)
    | f -> `Graph (x,f) in
  bool0#apply xss (aux x)
let aggreg op lz y (xs_opt,s) x =
  xs_opt,
  (fun args mode -> `Aggreg (op, x, y, lz, s args `Interrogative))

let func1_id = `Id
let pred2_eq = `Eq
let pred2_neq = `Neq
let pred2_geq = `Geq
let pred2_leq = `Leq
let pred2_gt = `Gt
let pred2_lt = `Lt
let proc1_return = `Return
let proc1_describe = `Describe
let aggreg_count = `Count
let modif_highest = `Highest
let modif_lowest = `Lowest

let whether s = bool0#whether s
let which x d1 d2 = bool0#which x (d1 x) (d2 x)
let exists x d = bool0#exists [x] (d x)
let forall x d1 d2 = bool0#forall [x] (d1 x) (d2 x)
let ifthen s1 s2 = bool0#forall [] s1 s2
let ifthenelse s1 s2 s3 = bool0#and_ [bool0#forall [] s1 s2; bool0#forall [] (bool0#not_ s1) s3]
let the x d1 d2 = bool0#the [x] (d1 x) (d2 x)
let where s1 s2 = bool0#the [] s2 s1
let a_number y d1 d2 x = aggreg aggreg_count [] y (bool0#and_ [d1 y; d2 y]) x
let modif op x = bool0#return (`Modif (op, x, `True))
(* let that (xs_opt,s) d = xs_opt, (fun args g mode -> s [] (fun t -> d t args g mode) mode) *)

let select xss = bool0#select xss

let tell xss = select xss [] `Affirmative

(* formula analysis and validation by adding iterators on resources *)

module VS =
  struct
    module M = Set.Make (struct type t = var let compare = Pervasives.compare end)
    include M

    let binder vs f = and_ (fold (fun v res -> `Triple (Var v, Uri "a", Uri "owl:Thing") :: res) vs [f])
    let rec add x vs =
      match x with
      | Var v -> M.add v vs
      | Uri _ -> vs
      | Literal _ -> vs
      | GraphLiteral (lv,lt) ->
	  let vs = List.fold_right (fun (s,p,o) vs -> add s (add p (add o vs))) lt vs in
	  let vs = List.fold_right M.remove lv vs in
	  vs
      | Cons (e,l) -> add e (add l vs)
    let add_list xs vs = List.fold_right add xs vs
    let list xs = add_list xs empty
    let rec remove x vs =
      match x with
      | Var v -> M.remove v vs
      | Uri _ -> vs
      | Literal _ -> vs
      | GraphLiteral (_lv,lt) -> List.fold_right (fun (s,p,o) vs -> remove s (remove p (remove o vs))) lt vs
      | Cons (e,l) -> remove e (remove l vs)
    let elements vs = fold (fun v res -> Var v :: res) vs []
    let rec is_bound x vs =
      match x with
      | Var v -> mem v vs
      | Uri _ -> true
      | Literal _ -> true
      | GraphLiteral (lv,lt) ->
	  let vs = List.fold_right M.add lv vs in
	  List.for_all (fun (s,p,o) -> is_bound s vs && is_bound p vs && is_bound o vs) lt
      | Cons (e,l) -> is_bound e vs && is_bound l vs
    let rec unbounds x vs =
      match x with
      | Var v -> if mem v vs then empty else singleton v
      | Uri _ -> empty
      | Literal _ -> empty
      | GraphLiteral (lv,lt) ->
	  let vs = List.fold_right M.add lv vs in
	  List.fold_right
	    (fun (s,p,o) res -> union (unbounds s vs) (union (unbounds p vs) (union (unbounds o vs) res)))
	    lt empty
      | Cons (e,l) -> union (unbounds e vs) (unbounds l vs)
    let unbounds_list xs vs =
      List.fold_right
	(fun x res -> union (unbounds x vs) res)
	xs empty
  end

let check_gen msg xs vs =
  let unbounds = VS.unbounds_list xs vs in
  if not (VS.is_empty unbounds)
  then failwith (msg ^ String.concat ", " (VS.M.elements unbounds))
let check_access = check_gen "Some references are out of scope: "
let check_bound = check_gen "Some references are unbound: "

type mode = Q | U

let rec validate = function
  | `And lf ->
      `And (List.map validate lf)
  | `Select (xs,f1) ->
      let acc = VS.list xs in
      let bound = VS.empty in
      let f1', bound' = validate_aux Q acc bound f1 in
      check_bound xs bound';
      `Select (xs,f1')
  | f ->
      let acc = VS.empty in
      let bound = VS.empty in
      let f', bound' = validate_aux U acc bound f in
      f'
and validate_aux mode acc bound f =
  match f with
  | `Triple (s,p,o) ->
      check_access [s;p;o] acc;
      f, VS.add_list [s;p;o] bound
  | `Unify (x,y) ->
      check_access [x;y] acc;
      ( match mode with
      | Q ->
	  ( match VS.is_bound x bound, VS.is_bound y bound with
	  | true, true -> `Pred2 (pred2_eq,x,y), bound
	  | true, false -> `Func (func1_id,[x],y), VS.add y bound
	  | false, true -> `Func (func1_id,[y],x), VS.add x bound
	  | false, false -> failwith "In equality, at least one side must be bound" )
      | U -> failwith "Equalities are not allowed in updates" )
  | `Matches (x,lre) ->
      check_access [x] acc;
      ( match mode with
      | Q -> check_bound [x] bound; f, bound
      | U -> failwith "Patterns are not allowed in updates" )
  | `Func (op,lx,y) ->
      check_access (y::lx) acc;
      ( match mode with
      | Q -> check_bound lx bound; f, VS.add y bound
      | U -> failwith "Functions are not allowed in updates" )
  | `Pred1 (op,x) ->
      check_access [x] acc;
      ( match mode with
      | Q -> check_bound [x] bound; f, bound
      | U -> failwith "Predicates are not allowed in updates" )
  | `Pred2 (op,x,y) ->
      check_access [x;y] acc;
      ( match mode with
      | Q -> check_bound [x;y] bound; f, bound
      | U -> failwith "Predicates are not allowed in updates" )
  | `Proc1 (op,x) ->
      check_access [x] acc;
      ( match mode with
      | Q -> failwith "Actions are not allowed in queries"
      | U -> check_bound [x] bound; f, bound )
  | `Graph (x,f1) ->
      check_access [x] acc;
      let f1', bound' = validate_aux mode acc bound f1 in
      `Graph (x,f1'), VS.add x bound'
  | `Aggreg (op,x,y,lz,f1) ->
      check_access [x] acc;
      ( match mode with
      | Q ->
	  (* like a SELECT lz y WHERE f1 *)
	  let acc1 = VS.add y (VS.add_list lz (VS.remove x acc)) in
	  let f1', bound' = validate_aux mode acc1 VS.empty (*bound*) f1 in (* HERE, bound not used because of SPARQL subqueries are isolated *)
	  check_bound (y::lz) bound';
	  (* after aggregation on y as x *)
(*	  let bound'' = VS.remove y (VS.inter acc1 bound') in *)
	  let bound'' = VS.remove y bound' in
	  let lz' = VS.elements bound'' (*VS.diff bound'' bound*) in
	  `Aggreg (op,x,y,lz',f1'), VS.add x (VS.union bound bound'') (*VS.add x bound''*)
      | U -> failwith "Aggregations are not allowed in updates" )
  | `Modif (op,x,f1) ->
      ( match mode with
      | Q ->
	  let f1', bound' = validate_aux mode acc bound f1 in
	  `Modif (op,x,f1'), bound'
      | U -> failwith "Superlatives are not allowed in updates" )
  | `Exists (xs,f1) ->
      let xs = List.filter (function Var v -> not (VS.M.mem v acc) | _ -> assert false) xs in (* because of lz in Aggreg *)
      if xs = []
      then validate_aux mode acc bound f1
      else begin
	let vs_xs = VS.list xs in
	if not (VS.is_empty (VS.inter acc vs_xs)) then failwith "A same variable is introduced twice: replace one by a reference";
	let acc1 = VS.union acc vs_xs in
	let f1', bound' = validate_aux mode acc1 bound f1 in
	`Exists (xs,f1'), VS.diff bound' vs_xs
      end
  | `Forall (`Exists (xs, f1), f2) ->
      let vs_xs = VS.list xs in
      if not (VS.is_empty (VS.inter acc vs_xs)) then failwith "A same variable is introduced twice: replace one by a reference";
      let acc1 = VS.union acc vs_xs in
      let f1', bound' = validate_aux Q acc1 bound f1 in
      let f2', bound'' = validate_aux mode acc1 bound' f2 in
      `Forall (`Exists (xs,f1'), f2'), bound
  | `Forall (f1,f2) ->
      let f1', bound' = validate_aux Q acc bound f1 in
      let f2', bound'' = validate_aux mode acc bound' f2 in
      `Forall (f1',f2'), bound
  | `True ->
      f, bound
  | `Not f1 ->
      let f1', bound' = validate_aux mode acc bound f1 in
      `Not f1', bound
  | `And lf ->
      let lf0, lf1, bound1 = 
	fold_while
	  (fun (lf0,lf1,bound) ->
	    if lf0 = []
	    then None
	    else
	      let lf0',f1,bound1 = validate_find mode acc bound lf0 [] (Failure "Some variable is unbound") in
	      Some (lf0',lf1@[f1],bound1))
	  (lf,[],bound) in
      assert (lf0 = []);
      `And lf1, bound1
  | `Or [] -> assert false
  | `Or (f1::lf1) ->
      let f1', b1' = validate_aux mode acc bound f1 in
      let rev_lf', bound' =
	List.fold_left
	  (fun (lf',bound') f ->
	    let f',b' = validate_aux mode acc bound f in
	    f'::lf', VS.inter b' bound')
	  ([f1'],b1') lf1 in
      `Or (List.rev rev_lf'), bound'
  | `Option f1 ->
      let f1', bound' = validate_aux mode acc bound f1 in
      `Option f1', bound' (* not bound because those variables can be used in SELECT *)
  | _ -> assert false
and validate_find mode acc bound lf0 rev_lf0' exn =
  match lf0 with
  | [] -> raise exn
  | f0::l ->
      try
	let f1, bound1 = validate_aux mode acc bound f0 in
	List.rev rev_lf0' @ l, f1, bound1
      with exn ->
	validate_find mode acc bound l (f0::rev_lf0') exn

let rec print = ipp
    [ f -> print_aux of f; EOF ]
and print_aux = ipp
    [ `Select (xs,f) -> "(select "; LIST0 print_term SEP " " of xs; " "; print_aux of f; ")"
    | `Triple (s,p,o) -> "(triple "; LIST1 print_term SEP " " of [s;p;o]; ")"
    | `Unify (x,y) -> "(unify "; LIST1 print_term SEP " " of [x;y]; ")"
    | `Func (op,lx,y) -> "(func "; print_op of op; " "; LIST0 print_term SEP " " of lx; " "; print_term of y; ")"
    | `Pred1 (op,x) -> "(pred1 "; print_op of op; " "; print_term of x; ")"
    | `Pred2 (op,x,y) -> "(pred2 "; print_op of op; " "; print_term of x; " "; print_term of y; ")"
    | `Proc1 (op,x) -> "(proc1 "; print_op of op; " "; print_term of x; ")"
    | `Graph (x,f) -> "(graph "; print_term of x; " "; print_aux of f; ")"
    | `Aggreg (op,x,y,lz,f1) -> "(aggreg "; print_op of op; " "; LIST1 print_term SEP " " of x::y::lz; " "; print_aux of f1; ")"
    | `Modif (op,x,f1) -> "(modif "; print_op of op; " "; print_term of x; " "; print_aux of f1; ")"
    | `Exists (xs,f) -> "(exists "; LIST1 print_term SEP " " of xs; " "; print_aux of f; ")"
    | `Forall (f1,f2) -> "(forall "; print_aux of f1; " "; print_aux of f2; ")"
    | `True -> "true"
    | `Not f1 -> "(not "; print_aux of f1; ")"
    | `And lf1 -> "(and "; LIST0 print_aux SEP " " of lf1; ")"
    | `Or lf1 -> "(or "; LIST0 print_aux SEP " " of lf1; ")"
    | `Option f1 -> "(option "; print_aux of f1; ")"
    | _ -> "(? ...)" ]
and print_op = ipp
    [ `Appl s -> 's
    | `Infix s -> 's
    | `Prefix s -> 's
    | `Id -> "id"
    | `Eq -> "="
    | `Geq -> ">="
    | `Leq -> "<="
    | `Gt -> ">"
    | `Lt -> "<"
    | `Return -> "return"
    | `Describe -> "describe"
    | `Count -> "count"
    | `Highest -> "highest"
    | `Lowest -> "lowest"
    | `Mod (order,offset,limit,boundness) -> "("; 'order; " "; print_int of offset; " "; print_int of limit; " "; 'boundness; ")" ]
and print_term = ipp
    [ Var s -> "?"; 's
    | Uri s -> 's
    | Literal s -> 's
    | GraphLiteral (lv,lt) -> "{ "; LIST0 print_var SEP " " of lv; " : "; MANY print_triple of lt; "}"
    | Cons (e,l) -> "["; print_term of e; "|"; print_term of l; "]" ]
and print_triple = ipp
    [ (s,p,o) -> print_term of s; " "; print_term of p; " "; print_term of o; " . " ]
and print_var = ipp [ v -> 'v ]
and print_int = ipp [ i -> '(string_of_int i) ]

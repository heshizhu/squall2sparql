(*
    This file is part of 'squall2sparql' <http://www.irisa.fr/LIS/softwares/squall/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

open Semantics

let ws = Syntax.ws
let kwd = Syntax.kwd
let kwds = Syntax.kwds

class ['s,'p] context =
  object (self)
    inherit ['s] Syntax.context

    method parse_pred1 : 'p = dcg
	[ "bound" -> `Appl "BOUND"
        | "URI" -> `Appl "isURI"
	| "IRI" -> `Appl "isIRI"
	| "blank" -> `Appl "isBLANK"
	| "literal" -> `Appl "isLITERAL"
	| "numeric" -> `Appl "isNumeric" ]
    method parse_func0 : 'p = dcg
	[ "bnode" -> `Appl "BNODE"
        | "random" -> `Appl "RAND"
	| "now" -> `Appl "now" ]
    method parse_pred2 : 'p = dcg
	[ "=" -> `Eq
        | "!=" -> `Neq
	| ">" -> `Gt
	| "<" -> `Lt
	| ">=" -> `Geq
	| "<=" -> `Leq
	| "sameTerm" -> `Appl "sameTerm"
	| _ = match "starts?[ ]+with" as "start(s) with" -> `Appl "strStarts"
	| _ = match "ends?[ ]+with" as "end(s) with" -> `Appl "strEnds"
	| _ = match "contains?" as "contain(s)" -> `Appl "contains"
	| _ = match "match\\(es\\)?" as "match(es)" -> `Appl "REGEX"
	| _ = match "lang-match\\(es\\)?" as "lang-match(es)" -> `Appl "langMatches" ]
    method parse_func1 : 'p = dcg
	[ _ = match "str\\(ings?\\)?" as "str(ing)" -> `Appl "str"
        | _ = match "lang\\(uages?\\)?" as "lang(uage)" -> `Appl "lang"
        | _ = match "datatypes?" as "datatype(s)" -> `Appl "datatype"
	| _ = match "lengths?" as "length(s)" -> `Appl "strlen"
	| _ = match "u\\(pper\\)?case" as "u(pper)case" -> `Appl "ucase"
	| _ = match "l\\(ower\\)?case" as "l(ower)case" -> `Appl "lcase"
	| _ = match "years?" as "year(s)" -> `Appl "year"
	| _ = match "months?" as "month(s)" -> `Appl "month"
	| _ = match "days?" as "day(s)" -> `Appl "day"
	| _ = match "hours" as "hours" -> `Appl "hours"
	| _ = match "minutes" as "minutes" -> `Appl "minutes"
	| _ = match "timezones?" as "timezone(s)" -> `Appl "timezone" 
	| _ = match "tz" as "tz" -> `Appl "tz"
	]
    method parse_addop : 'p = dcg
	[ _ = match "[ \t]+[+][ \t]+" as " + " -> `Infix "+"
	| _ = match "[ \t]+[-][ \t]+" as " - " -> `Infix "-" ]
    method parse_mulop : 'p = dcg
	[ _ = match "[ \t]+[*][ \t]+" as " * " -> `Infix "*"
	| _ = match "[ \t]+[/][ \t]+" as " / " -> `Infix "/" ]
    method parse_unop : 'p = dcg
	[ _ = match "+[ \t]*" as "+" -> `Prefix "+"
        | _ = match "-[ \t]*" as "-" -> `Prefix "-" ]
    method parse_func : 'p = dcg
	[ op = self#parse_func1 -> op
        | s = match "[A-Za-z_]+[0-9]*" as "func";
	    when "unknown function" List.mem (String.lowercase s)
		[ "uri"; "iri"; "bnode"; "strdt"; "strlang";
		  "strlen"; "substr"; "strbefore"; "strfater"; "encode_for_uri"; "concat"; "replace";
		  "abs"; "round"; "ceil"; "floor"; "rand";
		  "md5"; "sha1"; "sha256"; "sha384"; "sha512";
		]
	    -> `Appl s
	| uri = Syntax.parse_uri -> `Appl uri ]
    method parse_aggreg : 'p = dcg
	[ _ = kwd "count" -> `Appl "COUNT"
        | _ = kwd "number"; _ = ws; _ = kwd "of" -> `Appl "COUNT"
	| _ = kwds ["min"; "minimum"] -> `Appl "MIN"
	| _ = kwds ["max"; "maximum"] -> `Appl "MAX"
	| _ = kwds ["sum"; "total"] -> `Appl "SUM"
	| _ = kwd "average" -> `Appl "AVG"
	| _ = kwd "sample" -> `Appl "SAMPLE"
	| _ = kwd "concat" -> `Appl "GROUP_CONCAT" ]
    method parse_modif : 'p = dcg
	[ boundness = self#parse_boundness -> `Mod ("",0,-1, boundness)
        | order, offset, limit = self#parse_modif_order_offset_limit;
	  ( _ = ws; boundness = self#parse_boundness -> `Mod (order,offset,limit, boundness)
          |  -> `Mod (order,offset,limit,"") ) ]
    method private parse_modif_order_offset_limit = dcg
	[ "decreasing" -> ("DESC",0,-1)
        | "increasing" -> ("ASC",0,-1)
        | offset, limit = self#parse_offset_limit;
	  ( _ = ws; order = self#parse_order -> (order,offset,limit)
          |  -> ("",offset,limit) )
        | limit = Syntax.parse_nat; _ = ws;
	  ( _ = kwd "first" -> ("",0,limit)
	  | order = self#parse_order -> (order,0,limit) )
        | order = self#parse_order -> (order,0,1) ]
    method private parse_offset_limit = dcg
	[ ord1 = Syntax.parse_ordinal;
	  ( _ = ws; _ = kwd "to"; _ = ws;
	    ( ord2 = Syntax.parse_ordinal when "invalid modifier range" ord2 > ord1 -> ord1-1, ord2-ord1+1
            | _ = kwd "last" -> ord1-1, -1 )
          |  -> ord1-1, 1 ) ]
    method private parse_order = dcg
	[ _ = kwds ["greatest"; "highest"; "latest"] -> "DESC"
        | _ = kwds ["least"; "lowest"; "earliest"] -> "ASC" ]
    method private parse_boundness = dcg
	[ _ = kwds ["defined"; "bound"] -> "BOUND"
        | _ = kwds ["undefined"; "unbound"] -> "!BOUND" ]

  end

let rec transform = function
  | `And lf -> List.flatten (List.map transform lf)
  | `Select ([],f1) -> [`ASK f1]
  | `Select (xs,f1) -> [`SELECT (xs,f1)]
  | f -> transform_u f
and transform_u = function
  | `True -> []
  | `And lf1 -> List.filter ((<>) `NOP) (List.flatten (List.map transform_u lf1))
  | `Proc1 (op,x) -> transform_u (`Forall (`True, `Proc1 (op,x)))
  | `Forall (f1, `Proc1 (`Return, GraphLiteral (_lv,lt))) ->
      [`CONSTRUCT (lt,f1)]
  | `Forall (f1, `Proc1 (`Return, x)) ->
      let xs, f1 =
	match f1 with
	| `Exists (xs,f1) -> xs, f1
	| _ -> [], f1 in
      let xs1 = if List.mem x xs then xs else xs@[x] in
      [`SELECT ([x],f1)]
  | `Forall (f1, `Proc1 (`Describe, x)) -> [`DESCRIBE ([x],f1)]
  | `Forall (f1,f2) ->
      let i, d = transform_not f2 in
      [`MODIFY (i, d, f1)]
  | f ->
      match transform_not f with
      | `True, `True -> []
      | `True, d -> [`DELETE d]
      | i, `True -> [`INSERT i]
      | i, d -> [`INSERT i; `DELETE d]
and transform_not = function
  | `Graph (x, `True) -> transform_not `True
  | `Graph (x, `And lf1) ->
      let i, d = transform_not (`And lf1) in
      apply_graph x i, apply_graph x d
  | `Graph (x, `Not f1) ->
      let i, d = transform_not (`Not f1) in
      apply_graph x i, apply_graph x d
  | `True -> `True, `True
  | `And lf1 ->
      let li, ld = List.split (List.map transform_not lf1) in
      and_ li, and_ ld
  | `Not f1 -> `True, transform_quads f1
  | f -> transform_quads f, `True
and transform_quads = function
  | `True -> `True
  | `And lf1 -> and_ (List.map transform_quads lf1)
  | `Exists (xs,f1) -> `Exists (xs, transform_quads f1)
  | `Graph (x,f1) ->
      let i = transform_triples f1 in
      apply_graph x i
  | f -> transform_triples f
and apply_graph x = function
  | `True -> `True
  | f -> `Graph (x, f)
and transform_triples = function
  | `True -> `True
  | `And lf1 -> and_ (List.map transform_triples lf1)
  | `Exists (xs,f1) -> `Exists (xs, transform_triples f1)
  | `Triple _ as f -> f

  | `Func _
  | `Pred1 _
  | `Pred2 _
  | `Proc1 _
  | `Aggreg _ -> failwith "built-ins and aggregations are not allowed in updates"
  | `Modif _ -> failwith "solution modifiers are not allowed in updates"
  | `Graph _ -> failwith "nested graphs are not allowed in updates"
  | `Forall _ -> failwith "nested universals are not allowed in updates"
  | `Not _ -> failwith "nested negations are not allowed in updates"
  | `Or _ -> failwith "disjunctions are not allowed in updates"
  | `Option _ -> failwith "optionals are not allowed in updates"
  | _ -> failwith "invalid update"


(* generation of SPARQL *)

let space = ipp [ _ -> " " ]
let dot = ipp [ _ -> " . " ]
let print_int = ipp [ i -> '(string_of_int i) ]
let rec print_term = ipp
    [ Var "" -> "[]"
    | Var s -> "?"; 's
    | Uri s -> 's
    | Literal s -> 's
    | GraphLiteral _ -> '(failwith "graph literals are not supported in SPARQL")
    | Cons (e,l) -> "[ rdf:first "; print_term of e; " ; rdf:rest "; print_term of l; " ]" ]

let rec print = ipp
    [ ls -> LIST0 print_s SEP "\n" of ls; EOF ]
and print_s = ipp
    [ `ASK (`Exists (_, `Modif (`Mod (_,_,_,boundness),x,f))) -> "ASK { "; print_g_boundness of boundness, x, f; "}"
    | `ASK (`Modif (`Mod (_,_,_,boundness),x,f)) -> "ASK { "; print_g_boundness of boundness, x, f; "}"
    | `ASK f -> "ASK { "; print_g of f; "}"
    | `SELECT (xs, (`Modif _ as f)) -> print_modif of (xs,f)
    | `SELECT (xs, `Exists (_, (`Modif _ as f))) -> print_modif of (xs,f)
    | `SELECT (_, (`Aggreg _ as f)) -> print_aggreg of f
    | `SELECT (_, `Exists (_, (`Aggreg _ as f))) -> print_aggreg of f
    | `SELECT (xs, f) -> "SELECT DISTINCT "; LIST1 print_term SEP " " of xs; " WHERE { "; print_g of f; "}"
    | `DESCRIBE (xs, f) -> "DESCRIBE "; LIST1 print_term SEP " " of xs; [ `True ->  | f -> " WHERE { "; print_g of f; "}" ] of f
    | `CONSTRUCT (lt, f) -> "CONSTRUCT { "; MANY print_triple of lt; "} WHERE { "; print_g of f; "}"
    | f -> print_u of f ]
and print_u = ipp
    [ `INSERT i -> "INSERT DATA { "; print_g of i; "} "
    | `DELETE d -> "DELETE DATA { "; print_g of d; "} "
    | `MODIFY (i,d,g) ->
	[ d -> when d <> `True; "DELETE { "; print_g of d; "} " | _ -> ] of d;
	[ i -> when i <> `True; "INSERT { "; print_g of i; "} " | _ -> ] of i;
	[ g -> when g <> `True; "WHERE { "; print_g of g; "}" | _ -> ] of g ]
and print_g = ipp
    [ `Triple (s, Uri "rdf:element", o) -> print_triple of (s, Uri "rdf:rest*/rdf:first", o)
    | `Triple (s, Uri "rdf:last", o) -> print_term of s; space; print_term of (Uri "rdf:rest*"); space; "("; print_term of o; ")"; dot
    | `Triple (s,p,o) -> print_triple of (s,p,o)
    | `Matches (x,lre) ->
	"FILTER ("; LIST1 [ x, re -> "REGEX(str("; print_term of x; "),'"; 're; "','i')" ] SEP " && " of List.map (fun re -> (x,re)) lre; ")"; dot
(*	"FILTER (REGEX(str("; print_term of x; "),"; 're; ",'i') || EXISTS { "; print_term of x; " rdfs:label ?llsquall . FILTER REGEX(str(?llsquall),"; 're; ",'i') })"; dot *) (* requires Sparql 1.1: EXISTS *)
    | `Func (`Id, [Uri _ as x], (Var _ as y)) -> "VALUES "; print_term of y; " { "; print_term of x; " }"; dot
    | `Func (`Id, [Cons (e,l)], y) -> print_term of y; " rdf:first "; print_term of e; " ; rdf:rest "; print_term of l; dot
    | `Func (op,lx, (Var _ as y)) -> "BIND ("; print_expr of (op,lx); " AS "; print_term of y; ")"; dot
    | `Func (op,lx,y) -> "FILTER ("; print_expr of (op,lx); " = "; print_term of y; ")"; dot
    | `Pred1 (op, x) -> "FILTER "; print_pred1 of op, x; dot
    | `Pred2 (op,x,y) -> "FILTER "; print_pred2 of op, x, y; dot
    | `Graph (x,f) -> "GRAPH "; print_term of x; " { "; print_g of f; "} "
    | `Aggreg _ as f -> "{ "; print_aggreg of f; "} "
    | `Modif _ as f -> "{ "; print_modif of ([], f); "} "
    | `Exists (xs,f) -> print_g of f
    | `Forall (f1,f2) -> print_g of `Not (`And [f1; `Not f2])
    | `True -> 
    | `Not f -> "FILTER NOT EXISTS { "; print_g of f; "} "
    | `And lf -> MANY print_g of lf
    | `Or lf -> LIST1 [ f -> "{ "; print_g of f; "} " ] SEP "UNION " of lf
    | `Option f -> "OPTIONAL { "; print_g of f; "} " ]
and print_triple = ipp
    [ (s,p,o) -> print_term of s; space; print_predicate of p; space; print_term of o; dot ]
and print_predicate = ipp
    [ Uri "rdf:type" -> "a"
    | p -> print_term of p ]
and print_pred1 = ipp
    [ `Appl f, x -> 'f; "("; print_term of x; ")" ]
and print_pred2 = ipp
    [ `Appl f, x, y -> 'f; "("; print_term of x; ","; print_term of y; ")"
    | `Infix f, x, y -> "("; print_term of x; space; 'f; space; print_term of y; ")"
    | `Eq, x, y -> print_pred2 of `Infix "=", x, y
    | `Neq, x, y -> print_pred2 of `Infix "!=", x, y
    | `Geq, x, y -> print_pred2 of `Infix ">=", x, y
    | `Leq, x, y -> print_pred2 of `Infix "<=", x, y
    | `Gt, x, y -> print_pred2 of `Infix ">", x, y
    | `Lt, x, y -> print_pred2 of `Infix "<", x, y ]
and print_expr = ipp
    [ `Id, [x] -> print_term of x
    | `Prefix f, lx -> 'f; LIST0 print_term SEP " " of lx
    | `Infix f, lx -> let sep = " " ^ f ^ " " in LIST1 print_term SEP 'sep of lx
    | `Appl f, lx -> 'f; "("; LIST0 print_term SEP "," of lx; ")" ]
and print_aggreg = ipp
    [ `Aggreg (op, x, y, lz, f) ->
        "SELECT DISTINCT "; LIST0 print_term SEP " " of lz;
        " ("; print_aggreg_op of op; print_term of y; ") AS "; print_term of x;
        ") WHERE { "; print_g of f; "} ";
        [ [] ->  | lz -> "GROUP BY "; LIST0 print_term SEP " " of lz; space ] of lz ]
and print_aggreg_op = ipp
    [ `Appl g -> 'g; "("
    | `Count -> "COUNT(DISTINCT " ]
and print_modif = ipp
    [ xs, `Modif (op,x,f) ->
      "SELECT DISTINCT "; [ [] -> "*" | xs -> LIST1 print_term SEP " " of xs ] of xs;
      " WHERE { "; print_modif_op of (op,x,f) ]
and print_modif_op = ipp
    [ `Highest, x, f -> print_modif_op of `Mod ("DESC",0,1,""), x, f
    | `Lowest, x, f -> print_modif_op of `Mod ("ASC",0,1,""), x, f
    | `Mod (order,offset,limit,boundness), x, f -> print_g_boundness of boundness, x, f; "} "; print_order of order, x; print_offset of offset; print_limit of limit ]
and print_g_boundness = ipp
    [ "", x, f -> print_g of f
    | pred, x, f -> "OPTIONAL { "; print_g of f; "} FILTER ("; 'pred; "("; print_term of x; ")) " ]
and print_order = ipp
    [ "", x -> 
    | order, x -> "ORDER BY "; 'order; "("; print_term of x; ") " ]
and print_offset = ipp
    [ 0 -> 
    | offset -> "OFFSET "; print_int of offset; " " ]
and print_limit = ipp
    [ -1 -> 
    | limit -> "LIMIT "; print_int of limit ]

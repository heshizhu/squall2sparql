(*
    This file is part of 'squall2sparql' <http://www.irisa.fr/LIS/softwares/squall/>

    Sébastien Ferré <ferre@irisa.fr>, équipe LIS, IRISA/Université Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

open Eliom_pervasives
open HTML5.M
open Eliom_services
open Eliom_parameters
open Eliom_output.Html5


let sparql_of_squall = function squall ->
  let context = new Sparql.context in
  let _, sem =
    Dcg.once Syntax.parse context (Matcher.cursor_of_string Syntax.skip squall) in
  let sem = Semantics.validate sem in
  let cursor = Printer.cursor_of_formatter (Format.str_formatter) in
  Ipp.once Sparql.print (Sparql.transform sem) cursor context;
  Format.flush_str_formatter ()


let break_at_dot = function s ->
  let l = Str.full_split (Str.regexp_string " . ") s in
  let rec to_html l = match l with
    | Str.Text txt :: Str.Delim _ :: l' -> (pcdata (txt ^ " . ")) :: (br ()) :: (to_html l')
    | Str.Text txt :: [] -> [pcdata txt]
    | [] -> []
    | _ -> assert false
  in
  to_html l

let rec sparql_pp = function s ->
  let re = Str.regexp "\\([^{]*{\\)\\(.+\\)\\(}[^}]*\\)" in
  if Str.string_match re s 0 then
    let before = Str.matched_group 1 s in
    let middle = Str.matched_group 2 s in
    let after  = Str.matched_group 3 s in
    div ~a:[a_class ["indent"]] (List.concat [break_at_dot before;
                                              [sparql_pp middle];
                                              break_at_dot after])
  else
    div ~a:[a_class ["indent"]] (break_at_dot s)

let common_prologue =
  "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n" ^
  "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" ^
  "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" ^
  "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n"

let sparql_for_dbpedia sparql =
  common_prologue ^
  "PREFIX foaf: <http://xmlns.com/foaf/0.1/>\n" ^
  "PREFIX dc: <http://purl.org/dc/elements/1.1/>\n" ^
  "PREFIX res: <http://dbpedia.org/resource/>\n" ^
  "PREFIX dbo: <http://dbpedia.org/ontology/>\n" ^
  "PREFIX dbp: <http://dbpedia.org/property/>\n" ^
  "PREFIX : <http://dbpedia.org/ontology/>\n" ^
  sparql

(* ------------------------------------------------------------------ *)
(* Services *)

let main_service = Eliom_services.service ~path:[""] ~get_params:unit ()

let translate_service = Eliom_services.service
  ~path:["translate"]
  ~get_params:(Eliom_parameters.string "squall") ()

let query_form_service = Eliom_services.service ~path:["query_form"] ~get_params:unit ()

let query_service = Eliom_services.service
    ~path:["query"]
    ~get_params:(prod (string "endpoint")
		   (prod (string "output")
		      (prod (string "prologue")
			 (string "squall"))))
    ()

let examples_service = Eliom_services.service ~path:["examples"] ~get_params:unit ()

let squall_page = Eliom_services.external_service ~prefix:"http://www.irisa.fr" ~path:["LIS"; "softwares"; "squall"] ~get_params:unit ()
let bitbucket = Eliom_services.external_service ~prefix:"http://bitbucket.org" ~path:["sebferre"; "squall2sparql"] ~get_params:unit ()
let iswc04_paper = external_service ~prefix:"http://www.springerlink.com" ~path:["content"; "ftxy71qedrhb945v"] ~get_params:unit ()
let qald2 = external_service ~prefix:"http://greententacle.techfak.uni-bielefeld.de" ~path:["~cunger";"qald"] ~get_params:unit ()
let dbpedia_nsdecl = external_service ~prefix:"http://dbpedia.org" ~path:["sparql"] ~get_params:(string "nsdecl") ()
let dbpedia_sparql = external_service ~prefix:"http://dbpedia.org" ~path:["sparql"] ~get_params:(string "query") ()
let dbpedia_snorql = external_service ~prefix:"http://dbpedia.org" ~path:["snorql"] ~get_params:(string "query") ()

(* ------------------------------------------------------------------ *)
(* Some HTML that's repeated on every page *)
let menu =

  div ~a:[a_id "menu"]
    [ ul [ li [a ~service:main_service [pcdata "Translation form"] ()];
	   li [a ~service:query_form_service [pcdata "Query form"] ()];
           li [a ~service:examples_service  [pcdata "Examples"] ()];
           li [a ~service:bitbucket [pcdata "Download"] ()];
           li [a ~service:squall_page [pcdata "More information"] ()];
         ]
    ]

(* ------------------------------------------------------------------ *)
(* Handlers *)


(* This form is used by both handlers *)
let translate_form =
  let create_form = fun field_name ->
    [p [pcdata "Enter a SQUALL sentence: "; br ();
        string_input ~a:[a_size 100; a_autofocus `Autofocus] ~input_type:`Text ~name:field_name (); br ();
        string_input ~input_type:`Submit ~value:"Translate" ()]] in
  Eliom_output.Html5.get_form ~service:translate_service create_form

(* This form is used for querying distant SPARQL endpoints *)
let query_form =
  let create_form = fun (endpoint_name, (output_name, (prologue_name, squall_name))) ->
    [p [pcdata "Endpoint URL: "; br ();
	string_input ~a:[a_size 50; a_autofocus `Autofocus] ~input_type:`Text ~name:endpoint_name (); br ();
	pcdata "SPARQL Prologue: "; br ();
	textarea ~name:prologue_name ~rows:10 ~cols:100 ~value:common_prologue (); br ();
	pcdata "SQUALL sentence: "; br ();
	textarea ~name:squall_name ~rows:10 ~cols:100 (); br ();
	pcdata "Output: ";
	string_select ~name:output_name
	  (Option ([],"",None,false))
	  (List.map (fun (v,l) -> Option ([],v,Some (pcdata l),false))
	     ["xml", "XML";
	      "json", "JSON";
	      "text", "Text";
	      "csv", "CSV";
	      "tsv", "TSV"]);
	string_input ~input_type:`Submit ~value:"Get results" ()]] in
  Eliom_output.Html5.get_form ~service:query_service create_form

let webform_css =
  Eliom_output.Html5_forms.css_link
	~uri:(Eliom_output.Html5.make_uri (Eliom_services.static_dir ())
		    ["webform.css"]) ()


let translate_handler = fun (squall) () ->
  let sparql = sparql_of_squall squall in
  Lwt.return
   (html
    (head (title (pcdata "SQUALL to SPARQL Translator")) [webform_css])
    (body [menu;
           h1 [pcdata "SQUALL to SPARQL Translator"];
           h2 [pcdata "Your SQUALL sentence: "];
           p [pcdata squall];
           h2 [pcdata "The SPARQL translation: "];
           sparql_pp sparql;
           p [a ~service:dbpedia_sparql [pcdata "Run at DBpedia SPARQL endpoint"]
		(sparql_for_dbpedia sparql);
              pcdata " (assuming prefixes res: for resources, : and dbo: for ontology, and dbp: for properties in addition to ";
	      a ~service:dbpedia_nsdecl [pcdata "DBpedia namespace definitions"] "";
	      pcdata ")."];
           p [a ~service:dbpedia_snorql [pcdata "Load in DBpedia SPARQL Explorer"]
		(sparql_for_dbpedia sparql);
              pcdata " (assuming the same prefixes as above)."];
           hr ();
           translate_form]))

let query_handler = fun (endpoint, (output, (prologue, squall))) () ->
  let endpoint_prefix, endpoint_path =
    let n = String.length endpoint in
    try
      let i = String.rindex_from endpoint (n-2) '/' in
      String.sub endpoint 0 i, [String.sub endpoint (i+1) (n-i-1)]
    with Not_found ->
      endpoint, [] in
  let sparql = sparql_of_squall squall in
  let sparql = prologue ^ sparql in
  if output = ""
  then
    let external_service = external_service ~prefix:endpoint_prefix ~path:endpoint_path
	~get_params:(string "query") () in
    Lwt.return (preapply ~service:external_service sparql)
  else
    let external_service = external_service ~prefix:endpoint_prefix ~path:endpoint_path
	~get_params:(prod (string "output") (string "query")) () in
    Lwt.return (preapply ~service:external_service (output,sparql))
  

let main_handler = fun () () ->
  Lwt.return
    (html
       (head (title (pcdata "SQUALL to SPARQL Translator")) [webform_css])
       (body [menu;
              h1 [pcdata "SQUALL to SPARQL Translator"];
              p [pcdata "SQUALL (Semantic Query and Update High-Level Language) is a controlled natural language for querying and updating RDF graphs. SQUALL has a strong adequacy with RDF, and covers all constructs of SPARQL, and many of SPARQL 1.1."];
              p [pcdata "This demo page allows you to translate a SQUALL expression to SPARQL."];
              p [pcdata "SQUALL is designed and implemented by Sébastien Ferré. Please visit the ";
                 a ~service:squall_page [pcdata "official SQUALL web page"] ();
                 pcdata " for more information, source code and papers about SQUALL."];
              hr ();
              translate_form]))

let query_form_handler = fun () () ->
  Lwt.return
    (html
       (head (title (pcdata "SQUALL query form")) [webform_css])
       (body [menu;
	      h1 [pcdata "SQUALL query form"];
	      p [pcdata "This page allows you to query a SPARQL endpoint using the SQUALL language."];
	      p [pcdata "SQUALL is designed and implemented by Sébastien Ferré. Please visit the ";
                 a ~service:squall_page [pcdata "official SQUALL web page"] ();
                 pcdata " for more information, source code and papers about SQUALL."];
              hr ();
	      query_form]))

let examples_handler = fun () () ->
  let example_link txt = a ~service:translate_service [pcdata txt] txt in
  let example_cell txt = dd [example_link txt] in
  let example_item txt = li [example_link txt] in
  Lwt.return
    (html
       (head (title (pcdata "SQUALL to SPARQL Translator")) [webform_css])
       (body [menu;
              h1 [pcdata "SQUALL Examples"];
              h2 [pcdata "Language features"];
              p [pcdata "A number of the following examples were freely inspired by ";
                 em [a ~service:iswc04_paper [pcdata "A Comparison of RDF Query Languages"] ()];
                 pcdata " (Haase, Broekstra, Eberhart and Volz, ISWC'04)."];
              dl [ (dt [pcdata "Path expressions"], []),
                   (example_cell "What is the name of the author-s of X?", []);
                   (dt [pcdata "Union"], []),
                   (example_cell "What is the label of a topic or is the title of a publication?", []);
                   (dt [pcdata "Difference"], []),
                   (example_cell "What is the label of a topic and is not the title of a publication?", []);
                   (dt [pcdata "Optional"], []),
                   (example_cell "The author-s of a publication have which name and maybe have which email?",
		    [example_cell "What is the name of an author ?X of a publication and if defined, what is the email of ?X ?";]);
                   (dt [pcdata "Quantification"], []),
                   (example_cell "Which person is an author of every publication?",
                    [example_cell "Which person is an author of at least 10 publication-s?"]);
		   (dt [pcdata "Modifiers"], []),
		   (example_cell "Which mountain has the highest elevation ?",
		    [example_cell "Which mountain has an elevation that is the 2nd highest ?";
		     example_cell "Which person-s have the 10 lowest age ?";
		     example_cell "Which person-s have a child whose age is the 3rd to 5th lowest ?"]);
                   (dt [pcdata "Aggregation"], []),
                   (example_cell "How many person-s are an author of X?",
                    [example_cell "X has how many author-s?";
                     example_cell "What is the number of the author-s of X?";
                     example_cell "Which publication has how many author-s?";
		     example_cell "Which publication has at least 3 author-s?";
		     example_cell "Which publication has between 2 and 3 author-s?";
		     example_cell "Which publication has the most author-s?";
		     example_cell "Who is the author of the most publication-s?"]);
                   (dt [pcdata "Grouping"], []),
                   (example_cell "How many person-s have which affiliation ?",
		    [example_cell "What is the number of person-s per affiliation ?";
		     example_cell "How many publication-s have an author that has which affiliation?";
		     example_cell "What is the number of publication-s ?X per the affiliation of an author of ?X ?";
		     example_cell "What is the number of publication-s whose author has some affiliation ?affil per ?affil ?";
		     example_cell "What is the average number of publication-s per author ?";
		     example_cell "What is the average per ?journal of the count per ?publi of the author-s of a publication ?publi that is publishedIn a journal ?journal ?"]);
                   (dt [pcdata "Recursion"], []),
                   (example_cell "What is a subtopic+ of InformationSystems?", []);
                   (dt [pcdata "Entailment"], []),
                   (example_cell "What is a publication?",
                    [example_cell "What has a rdf:type that is rdfs:subClassOf* publication?"]);
		   (dt [pcdata "Reifications of predicates ('relate(s)') and classes ('belong(s)')"], []),
		   (example_cell "Which rdf:Property relates a publication to a person?",
		    [example_cell "To which nationality an author of X belongs?"]);
                   (dt [pcdata "Reification of triples"], []),
                   (example_cell "In which graph X has some topic?",
		    [example_cell "At source which publication X know-s Y?";
		     example_cell "Which person know-s Y at source at least 3 publication-s?"]);
		   (dt [pcdata "Patterns"], []),
		   (example_cell "How many publication-s have topic 'Computer Science' ?",
		    [example_cell "Which person whose rdfs:label is 'John Smith' is an author of X ?"]);
                   (dt [pcdata "Namespaces"], []),
                   (example_cell "Which organisation has a str that matches \"^http:/www.aifb.unikarlsruhe.de/\"?",
                    [example_cell "The string of which organisation matches \"^http:/www.aifb.unikarlsruhe.de/\"?"]);
                   (dt [pcdata "Language"], []),
                   (example_cell "What has language \"de\" and is the rdfs:label of the topic that has a rdfs:label whose str is \"Database Management\" and whose language is \"en\"?", []);
                   (dt [pcdata "Literals and Datatypes"], []),
                   (example_cell "Which publication has pageNumber 8?",
                    [example_cell "Which publication has a pageNumber whose str is \"08\"?"]);
		   (dt [pcdata "Expressions"], []),
		   (example_cell "Which rectangle has the heigth * the width equal to 100 and has 2 * (the height + the width) equal to 40 ?",
		    [example_cell "Return concat(\"Hello \", the foaf:firstName, \" \", the foaf:lastName, \" !\") of all person-s .";
		     example_cell "Which person has substr(the foaf:name,0,3) that contains \"Dr\" ?";
		     example_cell "Is every odd + every even an even ?"]);
                   (dt [pcdata "Collections"], []),
                   (example_cell "Which publication-s have authorList [A, B, C] ?",
		    [example_cell "Which publication-s have authorList [A, _, C] ?";
		     example_cell "What is an rdf:element of the authorList of X ?";
		     example_cell "X has authorList [..., what, ...] ?";
                     example_cell "What is the rdf:last of the authorList of X ?";
		     example_cell "X has authorList [..., what] ?"]);
		   (dt [pcdata "Imperative questions"], []),
		   (example_cell "Return the publication-s of X .",
		    [example_cell "List all author-s of X .";
		     example_cell "Give me the title of the publication-s of X ."]);
                   (dt [pcdata "Closed questions (ASK)"], []),
                   (example_cell "Whether X has an author that worksFor Y?",
		    [example_cell "Has X an author that worksFor Y ?"]);
		   (dt [pcdata "Describe questions (DESCRIBE)"], []),
		   (example_cell "Describe X.",
		    [example_cell "Describe every author of X."]);
		   (dt [pcdata "Graph literals as results (CONSTRUCT, N3 formulas)"], []),
		   (example_cell "For every person ?X that shares a parent with a person ?Y, return { ?X has sibling ?Y. ?Y has sibling ?X. }.",
		    [example_cell "For every person ?X, for ?S = concat(the firstname, \" \", the lastname) of ?X, return { ?X has fullname ?S. }.";
		     example_cell "If a person ?X shares a parent with a person ?Y then return { ?X has sibling ?Y and ?Y has sibling ?X. }."]);

                   (dt [pcdata "Insertion"], []),
                   (example_cell "X is a book whose title is \"A new book\" and whose author is Y.", []);
                   (dt [pcdata "Deletion"], []),
                   (example_cell "In graph G X has title \"Compiler Design\" and not \"Compiler desing\".", []);
                   (dt [pcdata "Group of updates"], []),
                   (example_cell "For every book ?b that some ?p relates to some ?v and whose date < \"2001-01-01\"^^xsd:date, ?p does not relate ?b to ?v.", 
		    [example_cell "If a book ?b has at least 2 author-s then ?b is a CollaborativeWork."]);
                   (dt [pcdata "Copy"], []),
                   (example_cell "Every ?p that relates some ?s to some ?v in graph G relates ?s to ?v in graph G2.", []);
                   (dt [pcdata "Move"], []),
                   (example_cell "Every ?p that relates some ?s to some ?v in graph G relates ?s to ?v in graph G2 and not G.", []);
                 ];

              h2 [pcdata "About films in DBpedia"];
              p [pcdata "The following examples are valid queries for the DBpedia SPARQL endpoint. To get results to those queries from DBpedia, just click the first link after the SPARQL translation. To make the writing of queries more concise and natural, the following prefix declarations are added to those of the Dbpedia SPARQL endpoint:"];
	      p [pcdata "PREFIX : <http://dbpedia.org/ontology/>"; br ();
		 pcdata "PREFIX res: <http://dbpedia.org/resource/>"; br ();
		 pcdata "PREFIX dbo: <http://dbpedia.org/ontology/>"; br ();
		 pcdata "PREFIX dbp: <http://dbpedia.org/property/>"];
	      ol [ example_item "List all Film-s.";
		   example_item "Which Film has director res:Tim_Burton and is starring res:Johnny_Depp and res:Helena_Bonham_Carter ?";
		   example_item "Which Film whose releaseDate is greater or equal to 2010-01-01 has a director whose birthPlace is res:England or res:United_States ?";
		   example_item "Which Film has country res:France or has a director or starring or musicComposer whose birthPlace is res:France ?";
		   example_item "Which Person has birthPlace res:United_States and is the director of a Film that is starring res:Johnny_Depp and whose releaseDate is what ?";
		   example_item "Which Person ?X is the director of a Film that is starring ?X ?";
		   example_item "Which Film has which country ?X and has a director whose birthPlace is not ?X ?";
		   example_item "How many Film-s whose director is res:Tim_Burton are starring which Person ?";
		 ];

	      h2 [pcdata "QALD DBpedia training questions"];
	      p [pcdata "The following examples are SQUALL translations for the DBpedia training questions from the ";
		 a ~service:qald2 [pcdata "QALD challenge"] ();
		 pcdata ", co-located with ESWC 2012. Those SQUALL translations have been designed to conform to the SPARQL solutions, and therefore may not be the simplest translation of the original English questions. We here assume the same prefixes as above."];
	      ol [ example_item "Give me all yago:RussianCosmonauts that are a yago:FemaleAstronauts.";
		   example_item "Give me the birthDate of the starring-s of res:Charmed?";
		   example_item "Who is the dbp:spouse of the child of res:Bill_Clinton?";
		   example_item "Which River does res:Brooklyn_Bridge cross?";
		   example_item "How many yago:EuropeanCountries have governmentType 'monarchy'?";
		   example_item "What is the deathPlace of res:Abraham_Lincoln?";
		   example_item "Is the spouse of res:Barack_Obama 'Michelle'?";
		   example_item "Which yago:StatesOfGermany have dbp:rulingParty 'SPD' or 'Social Democratic Party'?";
		   example_item "Which yago:StatesOfTheUnitedStates have dbp:mineral 'Gold'?";
		   example_item "What is the sourceCountry of res:Nile?";
		   example_item "Which Country is the location of more than 2 Cave-s?";
		   example_item "Is res:Proinsulin a Protein?";
		   example_item "What is the dbp:classis of res:Millipede?";
		   example_item "What is the height of res:Claudia_Schiffer?";
		   example_item "Who is the creator of res:Goofy?";
		   example_item "Give me the capital-s of the yago:AfricanCountries?";
		   example_item "Give me all City-es that isPartOf res:New_Jersey and whose populationTotal is greater than 100000.";
		   example_item "What is the dbp:museum of res:The_Scream?";
		   example_item "Is the largestCity of res:Egypt the capital of res:Egypt?";
		   example_item "What is the numberOfEmployees of res:IBM?";
		   example_item "What are the dbp:borderingstates of res:Illinois?";
		   example_item "What is the country of res:Limerick_Lake?";
		   example_item "Which TelevisionShow has creator res:Walt_Disney?";
		   example_item "Which Mountain has the highest elevation lesser than the elevation of res:Annapurna?";
		   example_item "Which Film has director res:Garry_Marshall and is starring res:Julia_Roberts?";
		   example_item "Which Bridge shares a dbp:design with res:Manhattan_Bridge?";
		   example_item "Has res:Andrew_Jackson a battle?";
		   example_item "Which yago:EuropeanCountries have governmentType res:Constitutional_monarchy?";
		   example_item "What are the dbp:awards of res:WikiLeaks?";
		   example_item "Which yago:StatesOfTheUnitedStates has the lowest dbp:densityrank?";
		   example_item "What is the currency of res:Czech_Republic?";
		   example_item "Which yago:EuropeanUnionMemberStates have dbp:currencyCode 'EUR'?";
		   example_item "What is the areaCode of res:Berlin?";
		   example_item "Which Country has more than 2 officialLanguage-s?";
		   example_item "Who is the owner of res:Universal_Studios?";
		   example_item "What is the dbp:country of res:Yenisei_River?";
		   example_item "What is the dbp:accessioneudate of res:Finland?";
		   example_item "Which yago:MonarchsOfTheUnitedKingdom have a spouse whose birthPlace is res:Germany?";
		   example_item "What is the date of res:Battle_of_Gettysburg?";
		   example_item "Which Mountain that is locatedInArea res:Australia has the highest elevation?";
		   example_item "Give me all SoccerClub whose dbp:ground is res:Spain.";
		   example_item "What are the officialLanguage-s of res:Philippines?";
		   example_item "What is the leaderName of res:New_York_City?";
		   example_item "Who is the dbp:designer of res:Brooklyn_Bridge?";
		   example_item "Which Organisation-s whose dbp:industry is 'Telecommunication' have location 'Belgium'?";
		   example_item "Has res:Frank_Herbert a deathDate?";
		   example_item "What is the highestPlace of res:Karakoram?";
		   example_item "Give me the foaf:homepage of res:Forbes.";
		   example_item "Give me all Company-es whose dbp:industry is 'advertising'.";
		   example_item "What is the deathCause of res:Bruce_Carver?";
		   example_item "Give me all yago:SchoolTypes.";
		   example_item "Which President-s or yago:Presidents have a birthDate that starts with \"1945\"?";
		   example_item "Give me all President-s whose dbp:title is res:President_of_the_United_States.";
		   example_item "Who is the spouse of res:Abraham_Lincoln?";
		   example_item "Who is the developer of res:World_of_Warcraft?";
		   example_item "What is the foaf:homepage of res:Tom_Cruise?";
		   example_item "List all TelevisionEpisode-s whose series is res:The_Sopranos and whose seasonNumber is 1.";
		   example_item "Who is the producer of the most Film-s?";
		   example_item "Give me all foaf:Person-s whose foaf:givenName is \"Jimmy\"@en.";
		   example_item "Is there a VideoGame that is 'Battle Chess'?";
		   example_item "Which Mountain has a greater dbp:elevationM than res:Nanga_Parbat?";
		   example_item "Who is the author of res:Wikipedia?";
		   example_item "Give me all starring-s of res:Batman_Begins.";
		   example_item "Which Software-s have developer an Organisation whose foundationPlace is res:California?";
		   example_item "Which Company has dbp:industry res:Aerospace or res:Nuclear_reactor_technology?";
		   example_item "Is res:Christian_Bale a starring of res:Batman_Begins?";
		   example_item "Give me the foaf:homepage-s of the Company-es whose dbp:numEmployees is greater than 500000.";
		   example_item "Which Actor-s have birthPlace res:Germany or a thing whose country is res:Germany?";
		   example_item "Which Cave-s have an dbp:entranceCount greater than 3?";
		   example_item "Give me all Film-s whose producer is res:Hal_Roach.";
		   example_item "Give me all VideoGame-s whose publisher is 'Mean Hamster Software'.";
		   example_item "What is a dbo:language of res:Estonia or is spokenIn res:Estonia?";
		   example_item "Who is the keyPerson of res:Aldi?";
		   example_item "Which yago:CapitalsInEurope are a yago:HostCitiesOfTheSummerOlympicGames?";
		   example_item "Who has orderInOffice '^5th President United States'?";
		   example_item "How many Film-s have producer res:Hal_Roach?";
		   example_item "What is the album of 'Last Christmas'?";
		   example_item "Give me all Book-s whose author is res:Danielle_Steel.";
		   example_item "Which Airport-s have location res:California?";
		   example_item "Give me all RecordLabel-s whose genre is res:Grunge and whose country is res:Canada.";
		   example_item "Which Country has the most dbp:officialLanguages?";
		   example_item "What is the programmingLanguage of res:GIMP?";
		   example_item "Who are the producer-s of the Film-s that are starring res:Natalie_Portman?";
		   example_item "Give me all Film-s that is starring res:Tom_Cruise.";
		   example_item "Which Film-s are starring res:Julia_Roberts and res:Richard_Gere?";
		   example_item "Give me all yago:FemaleHeadsOfGovernment whose dbp:office is res:Chancellor_of_Germany.";
		   example_item "Who is the author of res:The_Pillars_of_the_Earth?";
		   example_item "How many Film-s are starring res:Leonardo_DiCaprio?";
		   example_item "Give me all SoccerClub whose league is res:Premier_League.";
		   example_item "What is the dbp:foundation of res:Capcom?";
		   example_item "Which Organisation-s have a dbp:foundation or formationYear that starts with \"1950\"?";
		   example_item "Which Mountain has the highest elevation?";
		   example_item "Has res:Natalie_Portman a birthPlace whose country is res:United_States?";
		 ];

	      h2 [pcdata "QALD DBpedia test questions"];
	      p [pcdata "Same as above with DBpedia 'test' questions."];
	      ol [ example_item "Which City or Town whose country is res:Germany has a populationTotal greater than 250000?";
		   example_item "Who is the successor of 'John F. Kennedy'?";
		   example_item "Who is the leader of res:Berlin?";
		   example_item "What is the numberOfStudents of res:Vrije_Universiteit?";
		   example_item "Which Mountain has the 2nd highest elevation?";
		   example_item "Who has occupation 'Skateboarding' and has a birthPlace that is res:Sweden or has country res:Sweden?";
		   example_item "What is the dbp:admittancedate of res:Alberta?";
		   example_item "What are the country-s of 'Himalaya'?";
		   example_item "Who has occupation res:Bandleader and has instrument res:Trumpet?";
		   example_item "What is the dbp:strength of res:New_York_City_Fire_Department?";
		   example_item "Which FormulaOneRacer has the highest races?";
		   example_item "Which WorldHeritageSite has a dbp:year greater than 2007?";
		   example_item "Who has a team whose league is res:Premier_League and has the latest birthDate?";
		   example_item "Who is a bandMember of 'The Prodigy'?";
		   example_item "Which River has the greatest dbp:length?";
		   example_item "Has 'Battlestar Galactica 2004' a greater numberOfEpisodes than 'Battlestar Galactica 1978'?";
		   example_item "Which Automobile has dbp:production or dbp:assembly res:Germany or has a dbp:manufacturer that is a yago:AutomotiveCompaniesOfGermany and whose locationCountry is res:Germany?";
		   example_item "OUT OF SCOPE";
		   example_item "Who has birthPlace res:Vienna and has deathPlace res:Berlin?";
		   example_item "What is the height of res:Michael_Jordan?";
		   example_item "What is the capital of res:Canada?";
		   example_item "Who is the dbp:governor of res:Texas?";
		   example_item "Does 'Prince William' shares a dbp:mother with 'Prince Harry'?";
		   example_item "Who is the dbp:father of res:Elizabeth_II?";
		   example_item "Which yago:StatesOfTheUnitedStates has the latest dbp:admittancedate?";
		   example_item "res:Seychelles has how many officialLanguage-s?";
		   example_item "res:Sean_Parnell is the dbp:governor of which yago:StatesOfTheUnitedStates?";
		   example_item "Which Film-s have director res:Francis_Ford_Coppola?";
		   example_item "Who is a starring of a Film a director and a starring of which is res:William_Shatner?";
		   example_item "What is the dbp:birthName of res:Angela_Merkel?";
		   example_item "Which yago:CurrentNationalLeaders has dbp:religion res:Methodism?";
		   example_item "How many spouse-s of res:Nicole_Kidman are there?";
		   example_item "Which thing whose type is res:Nonprofit_organization has locationCountry res:Australia or has a location whose country is res:Australia?";
		   example_item "What are the battle-s of res:T._E._Lawrence?";
		   example_item "who is the developer of res:Skype?";
		   example_item "OUT OF SCOPE";
		   example_item "OUT OF SCOPE";
		   example_item "What is the populationTotal of res:Maribor?";
		   example_item "Which Company has location or headquarter or locationCity res:Munich?";
		   example_item "What has publisher 'GMT Games'?";
		   example_item "res:Intel is foundedBy who?";
		   example_item "Who is the dbp:spouse of res:Amanda_Palmer?";
		   example_item "What are the dbp:breed-s of res:German_Shepherd_Dog?";
		   example_item "What are the city-s of res:Weser?";
		   example_item "What are the country-s of res:Rhine?";
		   example_item "Who has occupation res:Surfing and has birthPlace res:Philippines?";
		   example_item "OUT OF SCOPE";
		   example_item "Which thing whose country is res:United_Kingdom is the headquarter of res:Secret_Intelligence_Service?";
		   example_item "Which Weapon shares the dbp:designer with res:Uzi?";
		   example_item "Has res:Cuban_Missile_Crisis a date later than the date of res:Bay_of_Pigs_Invasion?";
		   example_item "Which yago:FrisianIslands have country res:Netherlands?";
		   example_item "OUT OF SCOPE";
		   example_item "What is the dbp:leaderParty of res:Lisbon?";
		   example_item "What are the dbp:nickname-s of res:San_Francisco?";
		   example_item "Which yago:GreekGoddesses has dbp:abode res:Mount_Olympus?";
		   example_item "res:Hells_Angels are dbp:founded which?date?";
		   example_item "Who has mission res:Apollo_14?";
		   example_item "Give me the dbp:timezone of res:Salt_Lake_City?";
		   example_item "Which yago:StatesOfTheUnitedStates share the dbp:timezone with res:Utah?";
		   example_item "What is a Lake-s whose country is res:Denmark or is a yago:LakesOfDenmark?";
		   example_item "How many SpaceMission are there?";
		   example_item "Is res:Aristotle influencedBy res:Socrates?";
		   example_item "What is a yago:ArgentineFilms or is a Film whose country or dbp:country is 'Argentina'?";
		   example_item "Which LaunchPad has operator res:NASA?";
		   example_item "What are the instrument-s of res:John_Lennon?";
		   example_item "What has dbp:shipNamesake res:Benjamin_Franklin?";
		   example_item "Who are the parent-s of the spouse of 'Juan Carlos I'?";
		   example_item "What is the numberOfEmployees of res:Google?";
		   example_item "Has res:Nikola_Tesla award 'Nobel Prize Physics'?";
		   example_item "Is res:Michelle_Obama the spouse of res:Barack_Obama?";
		   example_item "Give me the dbp:beginningDate of res:Statue_of_Liberty.";
		   example_item "Which thing whose country is res:United_States is the location of res:Area_51?";
		   example_item "res:Benjamin_Franklin has how many child-s?";
		   example_item "What is the deathDate of res:Michael_Jackson?";
		   example_item "Which yago:DaughtersOfBritishEarls have birthPlace and deathPlace some?place?";
		   example_item "List the child-s of res:Margaret_Thatcher?";
		   example_item "Who has dbp:nickname res:Scarface?";
		   example_item "Has res:Margaret_Thatcher profession res:Chemist?";
		   example_item "Has res:Dutch_Schultz dbp:ethnicity 'Jewish'?";
		   example_item "Which Book has author res:William_Goldman and has a numberOfPages greater than 300?";
		   example_item "Which Book has publisher res:Viking_Press and has author res:Jack_Kerouac?";
		   example_item "Which yago:AmericanInventions are there?";
		   example_item "What is the elevation of res:Mount_Everest?";
		   example_item "Who is the creator of res:Captain_America?";
		   example_item "What is the populationTotal of the capital of res:Australia?";
		   example_item "What is the largestCity of res:Australia?";
		   example_item "Who is the musicComposer of res:Harold_and_Maude?";
		   example_item "Which Film has director and starring res:Clint_Eastwood?";
		   example_item "Which Settlement is the restingPlace of res:Juliana_of_the_Netherlands?";
		   example_item "What is the dbp:residence of res:Prime_Minister_of_Spain?";
		   example_item "Which yago:StatesOfTheUnitedStates has dbp:postalabbreviation 'MN'?";
		   example_item "Which Song has artist res:Bruce_Springsteen and has a releaseDate between 1980-01-01 and 1990-12-31?";
		   example_item "Which Film has director res:Sam_Raimi and has a greater releaseDate than res:Army_of_Darkness?";
		   example_item "What is the dbp:foundation of the dbp:brewery of res:Pilsner_Urquell?";
		   example_item "Who is the dbp:author of the anthem of res:Poland?";
		   example_item "What are the dbp:bSide-s of a thing whose musicalArtist is res:Ramones?";
		   example_item "Who is the dbp:artist of res:The_Storm_on_the_Sea_of_Galilee?";
		   example_item "What is the nationality of the creator of res:Miffy?";
		   example_item "What is the recordLabel of the Album whose artist is res:Elvis_Presley and whose releaseDate is the lowest?";
		   example_item "Who has product res:Orangina?";
		 ];

            ]))


(* ------------------------------------------------------------------ *)
(* Registration of services *)

let _ =
  Eliom_output.Html5.register main_service main_handler;
  Eliom_output.Html5.register translate_service translate_handler;
  Eliom_output.Html5.register query_form_service query_form_handler;
  Eliom_output.Redirection.register ~service:query_service query_handler;
  Eliom_output.Html5.register examples_service examples_handler


(* ------------------------------------------------------------------ *)
(* Catch some exceptions and output an error message instead of eg. the
   uninformative "error 500" page *)

let _ = Eliom_output.set_exn_handler
  (fun exn ->
    match exn with
      | Dcg.SyntaxError (line, column, msg) ->
        let get_params = Eliom_request_info.get_get_params () in
        let squall = List.assoc "squall" get_params in
        Eliom_output.Html5.send
          (html
             (head (title (pcdata "SQUALL to SPARQL Translator")) [webform_css])
             (body [menu;
                    h1 [pcdata "SQUALL to SPARQL Translator"];
                    h2 [pcdata "Syntax error"];
                    p [pcdata "The SQUALL expression you just entered is not syntactically correct."];
                    p [pcdata ("Error occured at line " ^ (string_of_int line) ^ ", ");
                       pcdata ("column " ^ string_of_int column);
                       pcdata " in the following expression:"];
                    div ~a:[a_id "faulty_squall"]
                      [pre [pcdata squall];
                       pre [pcdata ((String.make (column - 1) '-') ^ "^")]];
                    p [pcdata "The complete error message is: ";
                       pcdata msg];
                    p [pcdata "Please check against any typo and try again."];
                    translate_form
                   ]))
      | Failure msg ->
        let get_params = Eliom_request_info.get_get_params () in
        let squall = List.assoc "squall" get_params in
        Eliom_output.Html5.send
          (html
             (head (title (pcdata "SQUALL to SPARQL Translator")) [webform_css])
             (body [menu;
                    h1 [pcdata "SQUALL to SPARQL Translator"];
                    h2 [pcdata "Semantic error"];
                    p [pcdata "The SQUALL expression you just entered is not semantically correct."];
                    p [pcdata "Error occured in the following expression:"];
                    div ~a:[a_id "faulty_squall"]
                      [pre [pcdata squall]];
                    p [pcdata "The complete error message is: ";
                       pcdata msg];
                    p [pcdata "Please correct your sentence and try again."];
                    translate_form
                   ]))
      | Assert_failure _ ->
        let post_params = Eliom_request_info.get_all_post_params () in
        let squall = (match post_params with
          | Some params -> List.assoc "squall" params
          | _ -> "") in
        Eliom_output.Html5.send
          (html
             (head (title (pcdata "SQUALL to SPARQL Translator")) [webform_css])
             (body [menu;
                    h1 [pcdata "SQUALL to SPARQL Translator"];
                    h2 [pcdata "Broken assertion"];
                    p [pcdata "You ran into some corner case that shouldn't have happened. ";
                       pcdata "Please send the following information to ferre@irisa.fr:"];
                    p [pcdata ("The following SQUALL query: " ^ squall);
                       pcdata (" raised " ^ (Printexc.to_string exn))];
                    translate_form
                   ]))

      | _ -> Lwt.fail exn)

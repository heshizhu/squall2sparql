(*
    This file is part of 'squall2sparql' <http://www.irisa.fr/LIS/softwares/squall/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

open Semantics

class ['s] context =
  object (self)
    val var_cpt = 1
    method new_var =
      Var ("x" ^ string_of_int var_cpt),
      {< var_cpt = var_cpt+1 >}

    val sentence_start : Msg.Coord.t = (1,1)
    method set_sentence_start coord =
      {< sentence_start = coord >}
    method is_sentence_start coord = (coord = sentence_start)

    method is_kwd coord s =
      let s = if sentence_start = coord then String.uncapitalize s else s in
      List.mem s
	[ "a"; "all"; "an"; "and"; "are"; "as"; "at"; "average";
	  "be"; "belong"; "belongs"; "between"; "by";
	  "concat"; "count";
	  "decreasing"; "defined"; "describe"; "different"; "do"; "does";
	  "else"; "equal"; "each"; "earlier"; "earliest"; "every"; "exactly"; "exist"; "exists";
	  "first"; "for"; "from";
	  "graph"; "give"; "greater"; "greatest";
	  "has"; "have"; "higher"; "highest"; "how";
	  "if"; "in"; "increasing"; "is";
	  "last"; "later"; "latest"; "least"; "less"; "lesser"; "list"; "lower"; "lowest";
	  "many"; "max"; "maximum"; "maybe"; "me"; "min"; "minimum"; "more"; "most";
	  "nil"; "no"; "not"; "number";
	  "of"; "or"; "other";
	  "per";
	  "relate"; "relates"; "return";
	  "sample"; "share"; "shares"; "some"; "such"; "sum";
	  "than"; "that"; "the"; "then"; "there"; "thing"; "things"; "to"; "total";
	  "what"; "where"; "whether"; "which"; "with"; "who"; "whom"; "whose";
	]
  end
let new_var = dcg "new var" [ ?ctx; v, ctx' in "new_var" [ctx#new_var]; !ctx' -> v ]
let set_sentence_start = dcg "set sentence start" [ @cursor; ?ctx; ctx' in "new ctx" [ctx#set_sentence_start cursor#coord]; !ctx' -> () ]
let is_sentence_start = dcg "is sentence start" [ @cursor; ?ctx; when "" ctx#is_sentence_start cursor#coord -> () ]

let parse_pred1 = dcg [ ?ctx; op = ctx#parse_pred1 -> op ]
let parse_func0 = dcg [ ?ctx; op = ctx#parse_func0 -> op ]
let parse_pred2 = dcg [ ?ctx; op = ctx#parse_pred2 -> op ]
let parse_func1 = dcg [ ?ctx; op = ctx#parse_func1 -> op ]
let parse_addop = dcg [ ?ctx; op = ctx#parse_addop -> op ]
let parse_mulop = dcg [ ?ctx; op = ctx#parse_mulop -> op ]
let parse_unop = dcg [ ?ctx; op = ctx#parse_unop -> op ]
let parse_func = dcg [ ?ctx; op = ctx#parse_func -> op ]
let parse_aggreg = dcg [ ?ctx; op = ctx#parse_aggreg -> op ]
let parse_modif = dcg [ ?ctx; op = ctx#parse_modif -> op ]

let skip = Str.regexp ""

(* punctuations *)
let ws = dcg "space" [ s = match "[ \t\r\n]+" as "space" -> s ]
let comma = dcg "comma" [ s = match "[ \t\r\n]*,[ \t\r\n]*" as "comma" -> s ]
let semicolon = dcg "semicolon" [ s = match "[ \t\r\n]*;[ \t\r\n]*" as "semicolon" -> s ]
let dot = dcg "dot" [ s = match "[ \t\r\n]*[.]" as "dot" -> s ]
let interro = dcg "question mark" [ s = match "[ \t\r\n]*[?]" as "question mark" -> s ]
let left = dcg "left bracket" [ s = match "([ \t\r\n]*" as "left bracket"-> s ]
let right = dcg "right bracket" [ s = match "[ \t\r\n]*)" as "right bracket" -> s ]
let left_square = dcg "left square bracket" [ s = match "\\[[ \t\r\n]*" as "left square bracket" -> s ]
let right_square = dcg "right square bracket" [ s = match "[ \t\r\n]*\\]" as "right square bracket" -> s ]
let left_curly = dcg "left curly bracket" [ s = match "{[ \t\r\n]*" as "left curly bracket" -> s ]
let right_curly = dcg "right curly bracket" [ s = match "[ \t\r\n]*}" as "right curly bracket" -> s ]
let left_quote = dcg "left quote" [ s = match "'[ \t\r\n]*" as "left quote"-> s ]
let right_quote = dcg "right quote" [ s = match "[ \t\r\n]*'" as "right quote" -> s ]
let bar = dcg "bar" [ s = match "[ \t\r\n]*|[ \t\r\n]*" as "bar" -> s ]

(* grammatical morphemes and words *)
let kwd s =
  Dcg.alt
    (Matcher.look s)
    (Dcg.seq (Dcg.check "Capitalized words only at sentence start" (fun ctx cursor -> ctx#is_sentence_start cursor#coord))
       (fun _ -> Matcher.look (String.capitalize s)))

let rec kwds ls =
  match ls with
  | [] -> Dcg.fail
  | s::ls1 -> Dcg.alt (kwd s) (kwds ls1)

let a_an = dcg [ s = kwds ["a"; "an"] -> s ]
let a_the = dcg [ s = kwds ["a"; "an"; "the"] -> s ]
let does = dcg [ s = kwds ["does"; "do"] -> s ]
let be = dcg [ s = kwds ["is"; "are"; "be"] -> s ]
let have = dcg [ s = kwds ["has"; "have"] -> s ]
let share = dcg [ s = kwds ["shares"; "share"] -> s ]
let belong = dcg [ s = kwds ["belongs"; "belong"] -> s ]
let relate = dcg [ s = kwds ["relates"; "relate"] -> s ]
let at_in = dcg [ s = kwds ["at"; "in"] -> s ]
let of_by = dcg [ s = kwds ["of"; "by"] -> s ]
let what = dcg [ s = kwds ["what"; "who"; "whom"] -> s ]

(* words *)
let parse_var = dcg "var" [ "?"; s = match "[a-zA-Z0-9]+" as "variable" -> s ]
let re_prefix = "[A-Za-z]\\([A-Za-z_0-9.-]*[A-Za-z_0-9-]\\)?"
let re_ns = "\\(" ^ re_prefix ^ "\\)?:"
let re_local = "[A-Za-z_:0-9]\\([A-Za-z_0-9-.:]*[A-Za-z_0-9-:]\\)?"
let parse_uri = dcg "uri"
    [ s = match "<[^<>\"{}|^`\\\x00-\x20]*>" as "relative URI" -> s
    | s = match (re_ns ^ re_local) as "local name" -> s
    | @cursor; ?ctx; 
	s = match "[A-Za-z][A-Za-z_0-9]*" as "bare URI";
	  when '("reserved keyword: " ^ s) not (ctx#is_kwd cursor#coord s) -> ":" ^ s ]
let parse_nat = dcg "nat" [ s = match "[0-9]+" as "natural number" -> int_of_string s ]
let parse_ordinal = dcg "ordinal" [ n = parse_nat; _ = match "\\(st\\|nd\\|rd\\|th\\)" as "th" -> n ]
let parse_literal_nat = dcg [ n = parse_nat -> Literal (string_of_int n) ]
let parse_string = dcg
    [ s = match "\"\\([^\"\\\n\r]\\|[\\][tbnrf\"]\\)*\"" as "string" -> s
    | s = match "\"\"\"\\(\\(\"\\|\"\"\\)?\\([^\"\\]\\|[\\][tbnrf\"]\\)\\)*\"\"\"" as "long string" -> s ]
let re_date = "[0-9][0-9][0-9][0-9]-\\(0[1-9]\\|1[0-2]\\)-\\(0[1-9]\\|1[0-9]\\|2[0-9]\\|3[0-1]\\)"
let re_time = "\\([0-1][0-9]\\|2[0-3]\\):[0-5][0-9]:[0-5][0-9]\\(\\.[0-9]+\\)?"
let re_dateTime = re_date ^ "T" ^ re_time
let parse_literal = dcg "literal"
    [ _ = kwd "true" -> "true"
    | _ = kwd "false" -> "false"
    | s = match "[+-]?[0-9]*\\.[0-9]+" as "decimal" -> "\"" ^ s ^ "\"^^xsd:decimal"
    | s = match "[+-]?\\([0-9]+\\(\\.[0-9]*\\)?\\|\\.[0-9]+\\)[eE][+-]?[0-9]+" as "double" -> "\"" ^ s ^ "\"^^xsd:double"
    | s = match "[+-]?[0-9]+" as "integer" -> s
    | s = match re_dateTime as "dateTime" -> "\"" ^ s ^ "\"^^xsd:dateTime" 
    | s = match re_date as "date" -> "\"" ^ s ^ "\"^^xsd:date"
    | s = match re_time as "time" -> "\"" ^ s ^ "\"^^xsd:time"
    | s = parse_string;
      ( "@"; lang = match "[-a-zA-Z0-9]+" as "language tag" -> s ^ "@" ^ lang
      | "^^"; uri = parse_uri -> s ^ "^^" ^ uri
      |  -> s )
    | _ = kwd "the"; _ = ws; uri = parse_uri; _ = ws; s = parse_string -> s ^ "^^" ^ uri ]

(* Boolean closure *)
let rec parse_bool bool parse_atom = dcg
    [ la = LIST1 parse_and bool parse_atom SEP [ _ = ws; _ = kwd "or"; _ = ws -> () ] ->
        match la with [a] -> a | _ -> bool#or_ la ]
and parse_and bool parse_atom = dcg
    [ la = LIST1 parse_maybe bool parse_atom SEP [ _ = ws; _ = kwd "and"; _ = ws -> () ] ->
        match la with [a] -> a | _ -> bool#and_ la ]
and parse_maybe bool parse_atom = dcg
    [ _ = kwd "maybe"; _ = ws; a1 = parse_not bool parse_atom -> bool#maybe_ a1
    | _ = kwd "if"; _ = ws; _ = kwd "defined"; _ = comma; a1 = parse_not bool parse_atom -> bool#maybe_ a1
    | a1 = parse_not bool parse_atom -> a1 ]
and parse_not bool parse_atom = dcg
    [ _ = kwd "not"; _ = ws; a = parse_atom -> bool#not_ a
    | _ = left; a = parse_bool bool parse_atom; _ = right -> a
    | a = parse_atom -> a ]

(* Expressions *)
let rec parse_expr bool parse_atom = dcg
    [ e = parse_add bool parse_atom -> e ]
and parse_add bool parse_atom = dcg
    [ e1 = parse_mult bool parse_atom; e = parse_add_aux bool parse_atom e1 -> e ]
and parse_add_aux bool parse_atom e1 = dcg
    [ op = parse_addop; e2 = parse_mult bool parse_atom; x = new_var;
      e = parse_add_aux bool parse_atom
	(fun k -> e1 (fun x1 -> e2 (fun x2 -> bool#the [x] (bool#func op [x1; x2] x) (k x))))
	-> e
    |  -> e1 ]
and parse_mult bool parse_atom = dcg
    [ e1 = parse_unary bool parse_atom; e = parse_mult_aux bool parse_atom e1 -> e ]
and parse_mult_aux bool parse_atom e1 = dcg
    [ op = parse_mulop; e2 = parse_unary bool parse_atom; x = new_var;
      e = parse_mult_aux bool parse_atom
	(fun k -> e1 (fun x1 -> e2 (fun x2 -> bool#the [x] (bool#func op [x1; x2] x) (k x))))
	-> e
    |  -> e1 ]
and parse_unary bool parse_atom = dcg
    [ op = parse_unop; e1 = parse_primary bool parse_atom; x = new_var ->
        (fun k -> e1 (fun x1 -> bool#the [x] (bool#func op [x1] x) (k x)))
    | e1 = parse_primary bool parse_atom -> e1 ]
and parse_primary bool parse_atom = dcg
    [ _ = left; e = parse_add bool parse_atom; _ = right -> e
    | lit = parse_literal -> (fun k -> k (Literal lit))
    | var = parse_var;
      ( _ = match "[ \t\r\n]*=[ \t\r\n]*" as "="; e = parse_add bool parse_atom ->
	let x = Var var in
	(fun k -> e (fun x1 -> bool#the [x] (bool#func func1_id [x1] x) (k x)))
      |  -> (fun k -> k (Var var)) )
    | op = parse_func; _ = left; f = parse_args bool parse_atom; _ = right; x = new_var -> f op x
    | e = parse_atom -> e ]
and parse_args bool parse_atom = dcg
    [ e1 = parse_add bool parse_atom; f = parse_args_aux bool parse_atom ->
        (fun op x k -> e1 (fun x1 -> f op [x1] x k))
    |  -> (fun op x k -> bool#the [x] (bool#func op [] x) (k x)) ]
and parse_args_aux bool parse_atom = dcg
    [ _ = comma; ei = parse_add bool parse_atom; f = parse_args_aux bool parse_atom ->
        (fun op lx x k -> ei (fun xi -> f op (lx@[xi]) x k))
    |  -> (fun op lx x k -> bool#the [x] (bool#func op lx x) (k x)) ]

(* syntactical constructs *)
let rec parse = dcg
    [ _ = OPT ws ELSE ""; f = parse_text; _ = OPT ws ELSE ""; EOF -> f ]
and parse_text = dcg
    [ lf = LIST1 parse_s SEP ws -> and_ lf ]
and parse_s = dcg
    [ _ = set_sentence_start; s = parse_s_whether;
      ( _ = dot -> tell s
      | _ = interro -> tell (whether s) ) ]
and parse_s_whether = dcg
    [ _ = kwd "whether"; _ = ws; s = parse_s_for -> whether s
(*
    | _ = kwds ["return"; "list"; "give"; "give me"]; _ = ws; lxd = parse_npq_gen_and ["every"; "each"; "all"; "the"] ->
	List.fold_right (fun (x,d) s -> which x d (fun x -> s)) lxd bool0#true_
*)
    | s = parse_s_for -> s ]
and parse_s_for = dcg
    [ _ = kwd "if"; _ = ws; s1 = parse_s_for; _ = [ _ = comma -> () | _ = ws; _ = kwd "then"; _ = ws -> () ];
      s2 = parse_s_for;
      ( _ = ws; _ = kwd "else"; _ = ws; s3 = parse_s_for -> ifthenelse s1 s2 s3
      |  -> ifthen s1 s2 )
    | _ = kwd "for"; _ = ws; np = parse_np; _ = comma; s = parse_s_for -> np (fun x -> s)
    | _ = kwd "there"; _ = ws; _ = be; _ = ws; np = parse_np_gen parse_det_unary -> np (fun x -> bool0#true_)
    | s = parse_s_where -> s ]
and parse_s_where = dcg
    [ s1 = parse_s_pp;
      ( _ = ws; _ = kwd "where"; _ = ws; s2 = parse_s_for -> where s1 s2
      |  -> s1 ) ]
and parse_s_pp = dcg
    [ pp = parse_pp; _ = ws; s = parse_s_pp -> pp s
    | s = parse_s_bool -> s ]
and parse_s_bool = dcg
    [ s = parse_bool bool0 parse_s_atom -> s ]
and parse_s_atom = dcg
    [ _ = does; _ = ws; np = parse_np; _ = ws; vp = parse_vp_atom -> whether (np vp)
    | _ = be; _ = ws; 
      ( np = parse_np; _ = ws; vp = parse_vp_be -> whether (np vp)
      | _ = kwd "there"; _ = ws; np = parse_np_gen parse_det_unary -> whether (np (fun _ -> bool0#true_)) )
    | _ = have; _ = ws; np = parse_np; _ = ws; vp = parse_vp_have -> whether (np vp)
    | pr1 = parse_proc1; _ = ws; op = parse_op -> op (proc1 pr1)
    | np = parse_np; _ = ws; 
	( _ = does; _ = ws; np1 = parse_np; _ = ws; p2 = parse_p2; cp = parse_cp -> np (fun y -> np1 (fun x -> cp (p2 x y)))
        | _ = be; _ = ws; np1 = parse_np; _ = ws;
	  ( _ = a_the; _ = ws; m1 = parse_modif_opt; p2 = parse_p2; _ = ws; _ = kwd "of" ->
	    np (fun x -> np1 (fun y -> bool0#and_ [m1 y; p2 x y]))
	  | p2 = parse_p2; cp = parse_cp ->
	    np (fun y -> np1 (fun x -> cp (p2 x y))) )
        | vp = parse_vp -> np vp ) ]
and parse_graph = dcg
    [ s = parse_bool bool0 parse_graph_atom -> s ]
and parse_graph_atom = dcg
    [ _ = kwd "that"; _ = ws; s = parse_s_for -> s ]
and parse_np_gen parse_det = dcg
    [ lnp = LIST1 parse_np_gen_bool parse_det SEP comma -> bool1#and_ lnp ]
and parse_np_gen_bool parse_det = dcg
    [ np = parse_bool bool1 (parse_np_gen_expr parse_det) -> np ]
and parse_np_gen_expr parse_det = dcg
    [ e1 = parse_expr bool1
	(dcg [ np = parse_np_gen_atom parse_det -> (fun k1 d -> np (fun x -> k1 x d)) ]) ->
	  e1 (fun x1 d -> d x1) ]
and parse_np_gen_atom parse_det = dcg
    [ np = parse_np_gen_term (parse_np_gen_bool parse_det) -> np
    | "_"; x = new_var (* joker *) -> (fun d -> exists x d)
    | patt = parse_pattern; x = new_var -> (fun d -> exists x (bool1#and_ [d; patt]))
    | b = parse_b; x = new_var -> (fun d -> exists x (bool1#and_ [b; d]))
    | det = parse_det; _ = ws; x, p1 = parse_ng1 -> (fun d -> det x (init p1) d)
(*    | _ = kwd "which"; _ = ws; x, p1 = parse_ng1 -> (fun d -> which x (init p1) d) *)
    | np2 = parse_np2_gen parse_det; np = parse_p2' -> (fun d -> np (fun x -> np2 x d))
    | x, d1 = parse_npq_gen_atom ["which"] -> (fun d -> which x d1 d)
    | _ = what; x = new_var -> (* = which thing *)
	(fun d -> which x bool1#true_ d)
    | _ = kwd "whose"; x = new_var; _ = ws; y, p2 = parse_ng2 -> (* = the NG2 of what *)
	(fun d ->
	  which x bool1#true_ (fun x ->
	    exists y (bool1#and_ [init (p2 x); d]))) ]
(*    | _ = kwd "that"; _ = ws; s = parse_s_for -> (fun d -> that s d) ] *) (* deprecated *)

and parse_np2_gen parse_det = dcg
    [ np2 = parse_bool bool2 (parse_np2_gen_expr parse_det) -> np2 ]
and parse_np2_gen_expr parse_det = dcg
    [ e1 = parse_expr bool2
	(dcg [ np2 = parse_np2_gen_atom parse_det ->
	  (fun k1 x d -> np2 x (fun y -> k1 y x d)) ]) ->
	    e1 (fun x1 x d -> d x1) ]
and parse_np2_gen_atom parse_det = dcg
    [ det = parse_det; _ = ws; y, p2 = parse_ng2 ->
        (fun x d -> det y (init (p2 x)) d) ]
(*
    | _ = kwd "which"; _ = ws; y, p2 = parse_ng2 ->
	(fun x d -> which y bool1#true_ (bool1#and_ [init (p2 x); d])) ]
*)
and parse_np_gen_term parse_np = dcg "np_term"
    [ v = parse_var -> (fun d -> d (Var v))
    | s = parse_literal -> (fun d -> d (Literal s))
    | uri = parse_uri -> (fun d -> d (Uri uri))
    | _ = left_curly; f = parse_text; _ = right_curly -> (fun d -> d (graph_literal f))
    | _ = left_square;
	( _ = right_square -> (fun d -> d nil)
        | npl = parse_np_gen_list parse_np; _ = right_square -> npl ) ]
and parse_np_gen_list parse_np = dcg "np_list"
    [ np = parse_np;
      ( _ = comma; npl = parse_np_gen_list parse_np ->
	(fun dl -> np (fun e -> npl (fun l -> dl (cons e l))))
      | _ = bar; npl = parse_np ->
	(fun dl -> np (fun e -> npl (fun l -> dl (cons e l))))
      |   ->
	(fun dl -> np (fun x -> dl (cons x nil))) )
    | "..."; x = new_var;
      ( _ = comma; npl = parse_np ->
	(fun dl -> npl (fun l -> exists x (fun x -> bool0#and_ [triple x (Uri p2_sublist) l; dl x])))
      | _ = bar; npl = parse_np ->
	(fun dl -> npl (fun l -> exists x (fun x -> bool0#and_ [triple x (Uri p2_sublist) l; dl x])))
      |  ->
        (fun dl -> exists x dl) ) ]

and parse_npq_gen_and kwds_det = dcg
    [ lnpq = LIST1 parse_npq_gen_expr kwds_det SEP [ _ = ws; _ = kwd "and"; _ = ws -> () ] -> lnpq ]
and parse_npq_gen_expr kwds_det = dcg
    [ npq = parse_npq_gen_atom kwds_det -> npq
    | e1 = parse_expr bool0
	(dcg [ x, d = parse_npq_gen_atom kwds_det ->
	  (fun k0 -> bool0#and_ [d x; k0 x]) ]);
      x = [ _ = ws; _ = kwd "as"; _ = ws; x = parse_var -> Var x | x = new_var -> x ] ->
	x, (fun x -> e1 (fun x1 -> unify x1 x)) ]
and parse_npq_gen_atom kwds_det = dcg
    [ np = parse_np_gen_term (dcg [ x, d1 = parse_npq_gen_expr kwds_det -> (fun d -> exists x (bool1#and_ [d1; d])) ]); x = new_var ->
      x, (fun x -> np (fun x1 -> unify x1 x))
    | _ = kwds kwds_det; _ = ws;
      ( x, p1 = parse_ng1 -> x, p1
      | y, p2 = parse_ng2; _ = ws; _ = kwd "of"; _ = ws; np = parse_np -> (* = Det thing that is a NG2 of NP *)
	  y, (fun y -> np (fun x -> p2 x y)) ) ]

and parse_b = dcg
    [ _ = left_square; vp = OPT parse_vp ELSE bool1#true_; _ = right_square -> init vp ]
and parse_det_unary = dcg
    [ _ = a_an;
      ( _ = ws; x, g = parse_ngg; rel_opt = parse_rel_opt ->
	  (fun y d1 d2 -> exists x (bool1#and_ [g [] y (bool0#and_ [d1 y; d2 y]); rel_opt None]))
      |  -> (fun x d1 d2 -> exists x (bool1#and_ [d1; d2])) )
    | _ = kwd "the";
      ( _ = ws;
	( _ = kwd "most"; x = new_var ->
	  (fun y d1 d2 -> exists x (bool1#and_ [modif modif_highest; a_number y d1 d2]))
        | _ = kwd "least"; x = new_var ->
	  (fun y d1 d2 -> exists x (bool1#and_ [modif modif_lowest; a_number y d1 d2])) )
      |  -> (fun x d1 d2 -> the x d1 d2) )
    | _ = kwd "some" -> (fun x d1 d2 -> exists x (bool1#and_ [d1; d2]))
    | _ = kwd "no" -> (fun x d1 d2 -> bool0#not_ (exists x (bool1#and_ [d1; d2])))
    | _ = kwd "exactly"; _ = ws; n = parse_literal_nat; x = new_var ->
	(fun y d1 d2 -> exists x (bool1#and_ [a_number y d1 d2; pred2 pred2_eq n]))
    | _ = kwd "at"; _ = ws;
	( _ = kwd "least"; _ = ws; n = parse_literal_nat; x = new_var ->
	  (fun y d1 d2 -> exists x (bool1#and_ [a_number y d1 d2; pred2 pred2_leq n]))
        | _ = kwd "most"; _ = ws; n = parse_literal_nat; x = new_var ->
	  (fun y d1 d2 -> exists x (bool1#and_ [a_number y d1 d2; pred2 pred2_geq n])) )
    | _ = kwd "more"; _ = ws; _ = kwd "than"; _ = ws; n = parse_literal_nat; x = new_var ->
	(fun y d1 d2 -> exists x (bool1#and_ [a_number y d1 d2; pred2 pred2_lt n]))
    | _ = kwd "less"; _ = ws; _ = kwd "than"; _ = ws; n = parse_literal_nat; x = new_var ->
	(fun y d1 d2 -> exists x (bool1#and_ [a_number y d1 d2; pred2 pred2_gt n]))
    | _ = kwd "between"; _ = ws; n1 = parse_literal_nat; _ = ws; _ = kwd "and"; _ = ws; n2 = parse_literal_nat; x = new_var ->
	(fun y d1 d2 -> exists x (bool1#and_ [a_number y d1 d2; pred2 pred2_leq n1; pred2 pred2_geq n2]))
    | _ = kwd "how"; _ = ws; _ = kwd "many"; x = new_var ->
	(fun y d1 d2 -> which x bool1#true_ (a_number y d1 d2)) ]
and parse_det = dcg
    [ det = parse_det_unary -> det
    | _ = kwds ["every"; "all"] -> (fun x d1 d2 -> forall x d1 d2) ]
(*    | _ = kwd "which" -> (fun x d1 d2 -> which x d1 d2) ] *)
and parse_np = dcg
    [ np = parse_np_gen parse_det -> np ]
and parse_np2 = dcg
    [ np2 = parse_np2_gen parse_det -> np2 ]
and parse_ng1 = dcg
    [ op = parse_modif; x = parse_app_opt; rel_opt = parse_rel_opt -> x, rel_opt (Some (modif op))
    | m1 = parse_modif_opt;
      ( x = parse_var; rel_opt = parse_rel_opt -> Var x, rel_opt (Some m1)
      | p1 = parse_p1; x = parse_app_opt; rel_opt = parse_rel_opt -> x, rel_opt (Some (bool1#and_ [m1; p1]))
      | op = parse_aggreg; x = parse_app_opt; lz,y,s = parse_g' -> x, bool1#and_ [m1; aggreg op lz y s] ) ]
and parse_ng2 = dcg
    [ m1 = parse_modif_opt; p2 = parse_p2; y = parse_app_opt -> y, (fun x y -> bool0#and_ [m1 y; p2 x y]) ]
and parse_ngg = dcg
    [ m1 = parse_modif_opt; op = parse_aggreg; x = parse_app_opt -> x, (fun lz y s -> bool1#and_ [m1; aggreg op lz y s]) ]
and parse_modif_opt = dcg
    [ op = parse_modif; _ = ws -> modif op
    |  -> bool1#true_ ]
and parse_app_opt = dcg
    [ _ = ws; v = parse_var -> Var v
    | x = new_var -> x ]
and parse_rel_opt = dcg
    [ _ = ws; rel = parse_rel -> (function None -> rel | Some d -> bool1#and_ [d; rel])
    |  -> (function None -> bool1#true_ | Some d -> d) ]
and parse_rel = dcg
    [ rel = parse_bool bool1 parse_rel_atom -> rel ]
and parse_rel_atom = dcg
    [ _ = kwd "that"; _ = ws; vp = parse_vp -> init vp
    | _ = kwd "that"; _ = ws; np = parse_np; _ = ws; p2 = parse_p2; cp = parse_cp ->
	init (fun x -> np (fun y -> cp (p2 y x)))
    | _ = kwd "such"; _ = ws; _ = kwd "that"; _ = ws; s = parse_s_for -> init (fun x -> s)
    | np2 = parse_np2; _ = ws; _ = kwd "of"; _ = ws; _ = kwd "which"; _ = ws; vp = parse_vp ->
	init (fun x -> np2 x vp)
    | _ = kwd "whose"; _ = ws;
	( p2 = parse_p2; _ = ws; _ = be; _ = ws; np = parse_np; cp = parse_cp ->
	    init (fun x -> np (fun y -> cp (p2 x y)))
        | y, p2 = parse_ng2; _ = ws; vp = parse_vp ->
	    init (fun x -> exists y (bool1#and_ [p2 x; vp])) )
    | prep = parse_pseudo_prep; _ = ws; _ = kwd "which"; _ = ws; s = parse_s_for ->
	(fun z -> prep z s)
    | _ = at_in; _ = ws; _ = kwd "which"; _ = ws; prep = parse_prep; rel_opt = parse_rel_opt; _ = ws; s = parse_s_for ->
	rel_opt (Some (fun z -> prep z s))
    | comp = parse_comp_prefix; _ = ws; _ = parse_comp_suffix; _ = ws; op = parse_op -> init (fun x -> op (pred2 comp x))
    | _ = kwd "between"; _ = ws; op1 = parse_op; _ = ws; _ = kwd "and"; _ = ws; op2 = parse_op ->
        init (fun x -> bool0#and_ [op1 (pred2 pred2_geq x); op2 (pred2 pred2_leq x)]) ]
and parse_vp = dcg
    [ lvp = LIST1 parse_vp_pp SEP semicolon -> bool1#and_ lvp ]
and parse_vp_pp = dcg
    [ pp = parse_pp; _ = ws; vp = parse_vp_pp -> (fun x -> pp (vp x))
    | vp = parse_vp_bool -> vp ]
and parse_vp_bool = dcg
  [ vp = parse_bool bool1 parse_vp_aux -> vp ]
and parse_vp_aux = dcg
    [ _ = does; f1 = parse_aux_not; _ = ws; vp = parse_vp_atom -> f1 vp
    | _ = be; f1 = parse_aux_not_opt; _ = ws; vp = parse_vp_be -> f1 vp
    | _ = have; f1 = parse_aux_not_opt; _ = ws; vp = parse_vp_have -> f1 vp
    | vp = parse_vp_atom -> vp ]
and parse_aux_not_opt = dcg
    [ f1 = parse_aux_not -> f1
    |  -> bool1#id ]
and parse_aux_not = dcg
    [ "n't" -> bool1#not_
    | _ = ws; _ = kwd "not" -> bool1#not_ ]
and parse_vp_atom = dcg
    [ _ = share; _ = ws; vp = parse_vp_share -> vp
    | p1 = parse_p1; cp = parse_cp -> (fun x -> cp (p1 x))
    | p2 = parse_p2; _ = ws; op = parse_op -> (fun x -> op (p2 x)) ]
and parse_vp_be = dcg
    [ _ = kwd "there" -> bool1#true_
    | patt = parse_pattern -> patt
    | _ = a_an; _ = ws; m1 = parse_modif_opt; p1 = parse_p1; rel_opt = parse_rel_opt ->
	rel_opt (Some (bool1#and_ [m1; p1]))
    | _ = a_the; _ = ws; m1 = parse_modif_opt; p2 = parse_p2; np = parse_p2'; cp = parse_cp ->
	(fun y -> np (fun x -> bool0#and_ [m1 y; cp (p2 x y)]))
(*    | _, d = parse_ap -> d *)
(*    | np = parse_np_void -> (fun x -> np (fun y -> exists y (unify x))) *)
    | np = parse_np -> (fun x -> np (fun y -> unify x y))
    | rel = parse_rel -> rel ]
(*    | p2 = parse_p2; _ = ws; op = parse_op -> (fun x -> op (fun y -> p2 x y)) ] *) (* ambiguous *)
(*
	( op = parse_p2' -> (fun x -> op (fun y -> p2 y x))
        | _ = ws; op = parse_op -> (fun x -> op (fun y -> p2 x y)) )]
*)
and parse_vp_have = dcg
    [ _ = kwd "which"; _ = ws; y, p2 = parse_ng2; rel_opt = parse_rel_opt -> (* = P2 which thing Rel? *)
	(fun x -> which y (rel_opt None) (p2 x))
    | np2 = parse_np2; rel_opt = parse_rel_opt ->
	(fun x -> np2 x (rel_opt None))
    | p2 = parse_p2; _ = ws; op = parse_op ->
	(fun x -> op (fun y -> p2 x y))
    | _ = a_the; _ = ws; comp = parse_comp_prefix; _ = ws; p2 = parse_p2; _ = ws; _ = parse_comp_suffix;
	_ = ws; np = parse_np; y = new_var; y' = new_var ->
	  (fun x -> np (fun x' -> exists y (fun y -> exists y' (fun y' -> bool0#and_ [p2 x y; p2 x' y'; pred2 comp y y'])))) ]
and parse_comp_prefix = dcg
    [ _ = kwds ["same"; "equal"] -> pred2_eq
    | _ = kwds ["other"; "different"] -> pred2_neq
    | _ = kwds ["greater"; "higher"; "later"];
      ( _ = ws; _ = kwd "or"; _ = ws; _ = kwd "equal" -> pred2_geq
      |  -> pred2_gt )
    | _ = kwds ["lesser"; "lower"; "earlier"];
      ( _ = ws; _ = kwd "or"; _ = ws; _ = kwd "equal" -> pred2_leq
      |  -> pred2_lt ) ]
and parse_comp_suffix = dcg
    [ _ = kwds ["than"; "as"; "to"; "from"] -> () ]
and parse_vp_share = dcg
    [ det = parse_det; _ = ws; y, p2 = parse_ng2; _ = ws; _ = kwd "with"; _ = ws; np = parse_np ->
        (fun x1 -> det y (p2 x1) (fun y -> np (fun x2 -> p2 x2 y)))
    | _ = kwd "with"; _ = ws; np = parse_np; _ = ws; det = parse_det; _ = ws; y, p2 = parse_ng2 ->
	(fun x1 -> np (fun x2 -> det y (p2 x1) (fun y -> p2 x2 y))) ]
(* TODO: add (diff x1 x2) INTO np *)
(*
and parse_ap = dcg
    [ ap = parse_ap_atom -> ap ]
(*
    | e1 = parse_expr bool1
	(dcg [ _,d = parse_ap_atom; z = new_var -> (fun k1 x -> bool0#exists [z] (bool0#and_ [d z; k1 z x])) ]);
      x = [ _ = ws; _ = kwd "as"; _ = ws; x = parse_var -> Var x | x = new_var -> x ] ->
	x, e1 (fun x1 x -> unify x1 x) ]
*)
and parse_ap_atom = dcg
    [ _ = a_the; _ = ws;
      ( x, p1 = parse_ng1 -> x, p1
      | y, p2 = parse_ng2; op = parse_p2' -> y, (fun y -> op (fun x -> p2 x y)) ) ]
*)
and parse_op = dcg
    [ pp = parse_pp; _ = ws; op = parse_op -> (fun d -> pp (op d))
    | op = parse_op_bool -> op ]
and parse_op_bool = dcg
    [ op = parse_bool bool1 parse_op_atom -> op ]
and parse_op_atom = dcg
    [ np = parse_np; cp = parse_cp -> (fun d -> np (fun y -> cp (d y))) ]
and parse_cp = dcg
    [ cp = parse_bool bool1 parse_cp_atom -> cp ]
and parse_cp_atom = dcg
    [ _ = ws; pp = parse_pp; cp = parse_cp_atom -> (fun s -> pp (cp s))
    |  -> (fun s -> s) ]
and parse_pp = dcg
    [ pp = parse_bool bool1 parse_pp_atom -> pp ]
and parse_pp_atom = dcg
    [ prep = parse_pseudo_prep; _ = ws; np = parse_np ->
        (fun s -> np (fun z -> prep z s))
    | _ = at_in; _ = ws;
      ( prep = parse_prep; _ = ws; np = parse_np ->
	  (fun s -> np (fun z -> prep z s))
      | det = parse_det; _ = ws; prep = parse_prep; z = parse_app_opt; rel_opt = parse_rel_opt ->
	  (fun s -> det z (fun z -> prep z s) (rel_opt None))
      | _ = kwd "which"; _ = ws; prep = parse_prep; z = parse_app_opt; rel_opt = parse_rel_opt -> (* = at Prep which thing Rel? *)
	  (fun s -> which z (rel_opt None) (fun z -> prep z s)) ) ]
and parse_prep = dcg
    [ _ = kwd "graph" -> graph
    | uri = parse_uri -> arg uri ]
and parse_pseudo_prep = dcg
    [ _ = kwd "to" -> arg pseudo_prep_to ]
and parse_p2' = dcg
    [ _ = ws; _ = kwd "of"; _ = ws; np = parse_np -> np ]
and parse_g' = dcg
    [ y,d1 = parse_g'_of; lz,d2 = parse_per -> lz, y, bool0#and_ [d1 y; d2 y]
    | lz,d1 = parse_per; y,d2 = parse_g'_of -> lz, y, bool0#and_ [d1 y; d2 y]
    | _ = ws; y,p1 = parse_ng1; lz,d = parse_per -> lz, y, bool0#and_ [p1 y; d y]
    | _ = ws; y,p2 = parse_ng2;
      ( x, d1 = parse_g'_of; lz,d2 = parse_per ->
	lz, y, exists x (fun x -> bool0#and_ [p2 x y; d1 x; d2 x])
      | lz,d1 = parse_per; x,d2 = parse_g'_of ->
	lz, y, exists x (fun x -> bool0#and_ [p2 x y; d1 x; d2 x]) ) ]
and parse_g'_of = dcg
    [ _ = ws; _ = kwd "of"; _ = ws; x, d1 = parse_npq_gen_expr ["the"] -> x, d1 ]
and parse_per = dcg
    [ _ = ws; lap = LIST1 [ _ = kwd "per"; _ = ws; ap = parse_per_arg -> ap ] SEP comma ->
      let lz, ld = List.split lap in
      lz, bool1#and_ ld
    |  -> [], bool1#true_ ]
and parse_per_arg = dcg
    [ v = parse_var -> Var v, (fun y -> bool0#true_)
    | z, p2 = parse_ng2 -> z, (fun y -> p2 y z)
    | z, d = parse_npq_gen_expr ["the"] -> z, (fun y -> d z) ]
and parse_p1 = dcg
    [ p1 = parse_bool bool1 parse_p1_atom -> p1 ]
and parse_p1_atom = dcg
    [ _ = kwds ["thing"; "things"; "exists"; "exist"] -> bool1#true_
    | _ = belong; t = new_var -> a (Uri pseudo_p1_belongs) t
    | op = parse_pred1 -> pred1 op
    | op = parse_func0 -> func0 op
    | uri = parse_uri; _ = parse_p1_flexion; t = new_var -> a (Uri uri) t ]
and parse_p1_flexion = dcg
    [ s = match "\\(-e?s\\)?" -> s ]
and parse_p2 = dcg
    [ p2 = parse_bool bool2 parse_p2_expr -> p2 ]
and parse_p2_expr = dcg
    [ p2 = parse_p2_atom -> p2
    | e2 = parse_expr bool2
	(dcg [ p2 = parse_p2_atom; z = new_var ->
	  (fun k2 x y -> bool0#exists [z] (bool0#and_ [p2 x z; k2 z x y])) ]) ->
            e2 (fun x2 x y -> unify x2 y) ]
and parse_p2_atom = dcg
    [ _ = relate; t = new_var -> rel (Uri pseudo_p2_relates) t
    | op = parse_pred2 -> pred2 op
    | op = parse_func1 -> func1 op
    | _ = be; "-"; uri = parse_uri; t = new_var -> rel (Uri uri) t
    | pre = parse_p2_prefix; uri = parse_uri; _ = parse_p2_flexion; t = new_var -> rel (Uri (pre uri)) t
    | uri = parse_uri; _ = parse_p2_flexion; suf = parse_p2_suffix_opt; t = new_var -> rel (Uri (suf uri)) t ]
and parse_p2_flexion = dcg
    [ s = match "\\(-e?s\\|-e?d\\)?" -> s ]
and parse_p2_prefix = dcg
    [ "opt"; _ = ws;
      ( "trans"; _ = ws -> (fun uri -> uri ^ "*")
      |  -> (fun uri -> uri ^ "?") )
    | "trans"; _ = ws -> (fun uri -> uri ^ "+") ]
and parse_p2_suffix_opt = dcg
    [ s = match "[?+*]" as "property suffix" -> (fun uri -> uri ^ s)
    | s = match "{[0-9]+\\(,\\([0-9]+\\)?\\)?}" as "property suffix" -> (fun uri -> uri ^ s)
    | s = match "{,[0-9]+}" as "property suffix" -> (fun uri -> uri ^ s)
    |  -> (fun uri -> uri) ]
and parse_pattern = dcg "pattern"
    [ "'"; re = match "[^\n']+" as "pattern"; "'";
      lre in "invalid pattern" [Str.split (Str.regexp "[ \t]+") re]; when "empty pattern" lre <> [] -> matches lre ]
and parse_proc1 = dcg
    [ _ = kwd "return" -> proc1_return
    | _ = kwd "list" -> proc1_return
    | _ = kwd "give"; _ = ws; _ = kwd "me" -> proc1_return
    | _ = kwd "describe" -> proc1_describe ]
(*
and parse_term = dcg "term"
    [ v = parse_var -> Var v
    | s = parse_literal -> Literal s
    | uri = parse_uri -> Uri uri
    | _ = left_curly; f = parse_text; _ = right_curly -> graph_literal f
    | _ = kwd "nil" -> list_literal []
    | _ = left_square; l = LIST0 parse_term SEP comma; _ = right_square -> list_literal l ]
*)
